from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext


def log_graph(request):
    if request.user.is_staff:
        pass
    else:
        return HttpResponseRedirect(str(reverse('django.contrib.auth.views.login')) + '?next=' + request.path)

    loc = '/home/praveenr/logs/user/access_backpaper.log'
    #loc = 'C:\\Users\\Chitti\\Desktop\\access_backpaper.log'
    loc2 = loc + '.'
    #all_filenames = [loc,loc2+str(1),loc2+str(2),loc2+str(3),loc2+str(4),loc2+str(5),loc2+str(6),loc2+str(7)]
    all_filenames = [loc, loc2 + str(1),loc2+str(2)]
    result = []
    for filename in all_filenames:
        print filename
        current_result = {}
        current_result['name'] = filename[25:]
        hour_counter = {}
        fl = open(filename,'r')
        for each_line in fl.readlines():
            full_date = get_full_date(each_line)
            if full_date:
                time = get_local_time(get_time_informat(full_date))
                #print full_date +' -> '+str(time)
                try:
                    hour_counter[time.hour] = hour_counter[time.hour] + 1
                except Exception, e:
                    hour_counter[time.hour] = 1
            else:
                print "LOG_GRAPH : None Value at " + str(each_line)

        items = sorted(hour_counter.items())
        current_result['values'] = []
        for i in range(0,24):
            current_result['values'].append(get_item(i,items))

        result.append(current_result)
    return render_to_response(
        'indexv1/templates/log_graph.html',
        {
            'result': result,
        },
        context_instance=RequestContext(request))


import re
from time import strptime
from dateutil import tz
import datetime


def get_full_date(string):
    try:
        m = re.search('\d+\/[A-Z][a-z][a-z]\/\d+:\d+:\d+:\d+', string)
        return m.group()
    except Exception, e:
        print 'get_full_date : ' + str(e)
        return None


def get_time_informat(string):
    time = strptime(string, '%d/%b/%Y:%H:%M:%S')
    return time


def get_local_time(time):
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Asia/Calcutta')
    dt = datetime.datetime(*time[:6])
    utc = dt.replace(tzinfo=from_zone)
    local_time = utc.astimezone(to_zone)
    return local_time

def get_item(index,array):
    for each_item in array:
        if index == each_item[0]:
            return each_item[1]
    return 0
