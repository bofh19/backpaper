from django.shortcuts import render_to_response
from django.template import RequestContext	
from django.contrib.auth.decorators import login_required


from categories.models import *
from names.models import *


def index_index(request):
	if request.user.is_staff:
		admin_status = "block"
	else:
		admin_status = "none"
	return render_to_response(
                                'indexv1/templates/index_index2.html',
                                {
                                'admin_status_css':admin_status
                                },
                                context_instance=RequestContext(request))

mimetype = 'application/json'
from names.api import CategoriesResource
from names.api import NamesResource
from names.api import top_comment_cat_id as TOP_COMMENT_CAT_ID
import json
from django.core.cache import cache
from django.utils.encoding import smart_text
CACHE_TIMEOUT = 21600 #seconds

def index_1_index(request):
        if request.user.is_staff:
                admin_status = "block"
        else:
                admin_status = "none"
        try:
                cr_list_json = cache.get('cr_list_json')
                nr_final_list_json = cache.get('nr_final_list_json')
                if(cr_list_json and nr_final_list_json):
                        print "got both cache"
                        return render_to_response('indexv1/templates/index_index_1.html',{
                                'admin_status_css':admin_status,
                                'cr_list_json':cr_list_json,
                                'nr_list_json':nr_final_list_json,
                                },
                                context_instance=RequestContext(request))
        except Exception, e:
                print e
        cr = CategoriesResource()
        request_bundle = cr.build_bundle(request=request)
        queryset = cr.obj_get_list(request_bundle)
        bundles = []
        for obj in queryset:
                bundle = cr.build_bundle(obj=obj, request=request)
                bundles.append(cr.full_dehydrate(bundle, for_list=True))

        cr_list_json = cr.serialize(None, bundles, "application/json")
        
        nr = NamesResource()
        nr_final_list = {}
        cr_loaded = json.loads(cr_list_json)
        for each_cat in cr_loaded:
                this_get = {}
                this_get = request.GET.copy()
                this_get['categorie__id']=each_cat['id']
                this_get['device_height']=800
                this_get['device_width']=480
                if TOP_COMMENT_CAT_ID == each_cat['id']:
                        del this_get['categorie__id']
                        this_get['order_by'] = '-comments_count'
                        this_get['limit']=0

                request.GET = this_get
                request_bundle = nr.build_bundle(request=request)
                queryset = nr.obj_get_list(request_bundle)
                bundles = []
                for obj in queryset:
                        bundle = nr.build_bundle(obj=obj, request=request)
                        bundles.append(nr.full_dehydrate(bundle, for_list=True))

                nr_list_json = nr.serialize(None, bundles, "application/json")

                nr_final_list[each_cat['id']] = json.loads(nr_list_json)
        nr_final_list_json = json.dumps(nr_final_list)

        cache.set('cr_list_json',smart_text(cr_list_json),CACHE_TIMEOUT)
        cache.set('nr_final_list_json',smart_text(nr_final_list_json),CACHE_TIMEOUT)
        return render_to_response(
                'indexv1/templates/index_index_1.html',
                {
                'admin_status_css':admin_status,
                'cr_list_json':cr_list_json,
                'nr_list_json':nr_final_list_json,
                },
                context_instance=RequestContext(request))
#from django.
#def log_index(request):
#	from subprocess import Popen,PIPE
#	output = Popen('cat /home/praveenr/logs/user/access_backpaper.log',shell=True,stdout=PIPE).communicate()[0]
#	return Http

from django.http import HttpResponseRedirect
def main(request):
	return HttpResponseRedirect("/indexv1/")
import json
from django.http import HttpResponse
def policy(request):
	return render_to_response(
                                'indexv1/templates/policy.html',
                                {
                                
                                },
                                context_instance=RequestContext(request))


def categories_test(request):
	json_data = """[{"categorie_adult": false, "categorie_enabled": true, "categorie_name": "Top Commented", "categorie_power": 15, "created_by": "M", "id": 6, "resource_uri": "/api/v1/categories/6/"}, {"categorie_adult": false, "categorie_enabled": true, "categorie_name": "Hollywood Actress", "categorie_power": 11, "created_by": "A", "id": 2, "resource_uri": "/api/v1/categories/2/"}, {"categorie_adult": false, "categorie_enabled": true, "categorie_name": "Other", "categorie_power": 11, "created_by": "M", "id": 5, "resource_uri": "/api/v1/categories/5/"}, {"categorie_adult": false, "categorie_enabled": true, "categorie_name": "South Indian Actress", "categorie_power": 8, "created_by": "A", "id": 1, "resource_uri": "/api/v1/categories/1/"}, {"categorie_adult": false, "categorie_enabled": true, "categorie_name": "Bollywood", "categorie_power": 8, "created_by": "M", "id": 4, "resource_uri": "/api/v1/categories/4/"}, {"categorie_adult": true, "categorie_enabled": true, "categorie_name": "Porn Actress", "categorie_power": 0, "created_by": "A", "id": 3, "resource_uri": "/api/v1/categories/3/"}] """
	return HttpResponse(json_data,content_type="application/json")

def my_404_view(request):
	return render_to_response(
                                'indexv1/templates/404.html',
                                {

                                },
                                context_instance=RequestContext(request))

