__author__ = 'w4rlock'

from django.contrib import admin

from utils.models import cvImagesDirection
from sorl.thumbnail import get_thumbnail
from images.models import name_images

class cv_images_direction_info_admin(admin.ModelAdmin):
	model = cvImagesDirection
	list_display=('id','image_id','crop_direction','crop_direction_int','link_to_image')
	list_filter = ['crop_direction']
	list_per_page = 500
	def crop_direction_int(self,obj):
		try:
			return str(obj.crop_direction) + " current direction "+str(obj.image_id.crop_direction)
		except Exception, e:
			return "something failed"
	def link_to_image(self,obj):
		BANNER_THUMBS_SIZE_X = '480x800'
		return_str = ''
		crop_direction = 'center'
		if obj.image_id.crop_direction != 0:
			if(obj.image_id.crop_direction == 1):
				crop_direction = 'right'
			elif(obj.image_id.crop_direction == -1):
				crop_direction = 'left'
			try:
				return_str = get_thumbnail(obj.image_id.imagefile, BANNER_THUMBS_SIZE_X,crop=crop_direction).url
				return_str = "<a href='/admin/images/name_images/%s' target='_blank'><img src='%s' /></a>"%(obj.image_id.id,return_str)
				return return_str
			except Exception, e:
				#print e
				return_str = "<a href='/admin/images/name_images/%s' target='_blank'>unable to crop</a>"%(obj.image_id.id)
		else:
			return "no need to crop"
	link_to_image.short_description = "link to image"
	link_to_image.allow_tags = True

admin.site.register(cvImagesDirection,cv_images_direction_info_admin)