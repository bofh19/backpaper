from django.db import models
from images.models import name_images

class cvImagesDirection(models.Model):
    image_id = models.ForeignKey(name_images,related_name='image_id_fk',unique=True)
    right = 1
    center = 0
    left = -1
    CROP_SIDE_CHOICES = (
    (right, u'Right'),
    (center, u'Center'),
    (left, u'Left'),
    )
    crop_direction = models.IntegerField(verbose_name=("Crop Direction"), choices=CROP_SIDE_CHOICES,default=center)

    def __unicode__(self):
        return self.image_id.name.name
