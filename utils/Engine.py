__author__ = 'w4rlock'

# assumes some user is authenticated authorized
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import slugify


from images.models import name_images
from names.models import Name
from categories.models import Categories

from django.contrib.auth.models import User

import urllib2

HEADERS = {
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Encoding":"gzip,deflate,sdch",
"Accept-Language":"en-US,en;q=0.8,te;q=0.6",
"Cache-Control":"max-age=0",
"Connection":"keep-alive",
"User-Agent":"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",
}

HEADERS_WALL_SITE = {
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Encoding":"gzip,deflate,sdch",
"Accept-Language":"en-US,en;q=0.8,te;q=0.6",
"Cache-Control":"max-age=0",
"Connection":"keep-alive",
"User-Agent":"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",	
}


class ImageSaveEngine:
	def __init__(self,headers=False):
		self.headers = headers

	def save(self,url,name,user,categorie='None',context_url=""):
		return self.save_image_from_url(url,name,user,categorie,context_url)

	def save_image_from_url(self,url,name,user,categorie='None',context_url=""):
		"""
		inputs: url,name,user,cateorie[not required]
		creates name for that specific categorie if does not exist
		saves the url's image into that specific name
		returns [-1,error_code,desc,url] on fail
		returns [1,sucess_code,path,url] on sucess
		error codes:
		 0 - unknown status check desc
		 1 - failed on downloading image
		 2 - unable to create name/categorie
		 3 - unable to save
		 4 - unable to get username
		 100 - sucessfully saved image 
		"""
		try:
			f = File(self._handle_upload_url_file(url))
		except Exception, e:
			return [-1,1,str(e),url]
		new_image = name_images()
		try:
			new_image.name = self._get_name_instance(name,categorie)
		except Exception, e:
			return [-1,2,str(e),url]

		try:
			new_image.source = url
			filename = url.split('/')[-1]
			slug_filename = self._get_proper_filename(filename)
			new_image.image_adult = new_image.name.categorie.categorie_adult
		except Exception, e:
			return [-1,0,str(e),url]

		try:
			new_image.uploaded_by = User.objects.get(username=user)
		except Exception, e:
			return [-1,5,str(e),url]

		try:
			new_image.context_source = context_url
			new_image.imagefile.save(slug_filename,f)
		except Exception, e:
			return [-1,3,str(e),url]
		
		return [1,100,new_image.imagefile.path,url]


	def _get_proper_filename(self,filename):
		ax = filename.split('.')
		if len(ax) >=2 :
			file_ext = '.'+filename.split('.')[-1]
			slug_filename = slugify(filename.replace(file_ext,''))+file_ext
		else:
			slug_filename = slugify(filename)
		slug_filename = slug_filename.replace('-','_')
		return slug_filename

	def _handle_upload_url_file(self,url):
		try:
			img_temp = NamedTemporaryFile()
			if(self.headers):
				request = urllib2.Request(url,headers=HEADERS_WALL_SITE)
			else:
				request = urllib2.Request(url)
			img_temp.write((urllib2.urlopen(request)).read())
			img_temp.flush()
		except Exception, e:
			raise e
		return img_temp

	def _get_name_instance(self,name_given,categorie_given):
		name_req = name_given
		if (name_req == '' or name_req == ' '):
			print "no name_req given for new name"
			message = "no name_req given for new name"
			return -1
			# return -1
		try:
			name_instance = Name.objects.get(name=name_req)
			return name_instance
		except ObjectDoesNotExist, e:
			cat_req = categorie_given
			if (cat_req == '' or cat_req == ' ' or cat_req=='None'):
				#print "no catagorie given for new name"
				raise Exception("No Name Given")
			try:
				cat_instance = Categories.objects.get(categorie_name = cat_req)
				print "got cat "
				print cat_instance
			except ObjectDoesNotExist, e:
				new_cat_instance = Categories()
				new_cat_instance.categorie_name = cat_req
				new_cat_instance.created_by = 'A'
				new_cat_instance.save()
				cat_instance = new_cat_instance
			except Exception,e:
				print "something went wrong seriously while getting cat_instance"
				raise e
				#return -1

			new_name_instance = Name()
			new_name_instance.categorie = cat_instance
			new_name_instance.name = name_req
			new_name_instance.created_by = 'A'
			new_name_instance.save()
			return new_name_instance
			
		except Exception, e:
			print "something went wrong seriously  while getting name_instance"
			raise e
			#return -1

		return name_instance

from bs4 import BeautifulSoup as bs
import urllib2
import urlparse
# class GetUrlData:
# 	def __init__(self,url,depth_count=1,headers=False):
# 		self.given_url = url
# 		self.urlp = urlparse.urlparse(url)
# 		self.depth_properties = {}
# 		self.depth_urls = {}
# 		self.headers = headers
# 		self.depth_count = depth_count
# 		#self.data = None
# 		#self._set_main_raw_data()
	
# 	def set_depth(self,depth):
# 		self.depth = depth

# 	def set_headers(self,headers):
# 		self.headers = headers
	
# 	def set_level_src_prop(self,dom_type,attr_type,attr_name,dom_src,levelnum):
# 		itemx = {}
# 		itemx['dom_type'] =  dom_type
# 		itemx['attr_type'] = attr_type
# 		itemx['attr_name'] = attr_name
# 		itemx['dom_src'] = dom_src
# 		self.depth_properties[levelnum] = itemx

# 	def _get_items_main(self):
# 		for each_m_url in self._get_all_main_page_urls():
# 			x = self.__inner_url__(each_m_url,self.depth_properties[1],self.headers)
# 			cur_sub_urls = []
# 			for each_url in x.get_urls():
# 				urlpx = urlparse.urlparse(each_url)
# 				if(not urlpx.hostname):
# 					cur_sub_urls.append(self.urlp.scheme+"://"+self.urlp.hostname+each_url)
# 				else:
# 					cur_sub_urls.append(urlpx)
# 		self.depth_urls[1] = cur_sub_urls
# 		self._get_next_depth(1)

# 	# TODO make this to return all pages
# 	def _get_all_main_page_urls(self):
# 		return [self.given_url]

# 	def _get_next_depth(self,depth_num):
# 		urls = self.depth_urls[depth_num]
# 		cur_sub_urls_main = {}
# 		for each_url in urls:
# 			x = self.__inner_url__(each_m_url,self.depth_properties[depth_num+1],self.headers)
# 			cur_sub_urls = []
# 			for each_url in x.get_urls():
# 				urlpx = urlparse.urlparse(each_url)
# 				if(not urlpx.hostname):
# 					cur_sub_urls.append(self.urlp.scheme+"://"+self.urlp.hostname+each_url)
# 				else:
# 					cur_sub_urls.append(urlpx)
# 			cur_sub_urls_main[each_url] = cur_sub_urls
# 		self.depth_urls[depth_num+1] = cur_sub_urls_main


# 	# # internal funcs
# 	# def _set_main_raw_data(self):
# 	# 	self.data = self.get_raw_data(self.given_url,self.headers)



# 	# def get_raw_data(self,url,headers):
# 	# 	if headers:
# 	# 		request = urllib2.Request(url,headers=HEADERS_WALL_SITE)
# 	# 		return (urllib2.urlopen(request)).read()
# 	# 	else:
# 	# 		return (urllib2.urlopen(url)).read()

# 	# def get_class_item(self,data,dom_type,class_name):
# 	# 	x = bs(data)
# 	# 	return x.find_all(dom_type,attrs={'class':class_name})


# 	class __inner_url__:
# 		def __init__(self,url,depth_props,headers=False):
# 			self.data = None
# 			self.given_url = url
# 			self.urlp = urlparse.urlparse(url)
# 			self.headers = headers
# 			self.depth_props = depth_props
# 			self._set_main_raw_data()

# 		def _set_main_raw_data(self):
# 			print "..",self.given_url
# 			self.data = self._get_raw_data(self.given_url,self.headers)

# 		def get_urls(self):
# 			x = bs(self.data)
# 			attrs = x.find_all(self.depth_props['dom_type'],attrs={self.depth_props['attr_type']:self.depth_props['attr_name']})
# 			urls = []
# 			for each_attr in attrs:
# 				urls.append(each_attr[self.depth_props['dom_src']])
# 			return urls

# 		def _get_raw_data(self,url,headers):
# 			if headers:
# 				request = urllib2.Request(url,headers=HEADERS_WALL_SITE)
# 				return (urllib2.urlopen(request)).read()
# 			else:
# 				return (urllib2.urlopen(url)).read()

from bs4 import BeautifulSoup as bs
import urllib2
import urlparse
class GetImageBaseClass:
	from bs4 import BeautifulSoup as bs

	def execute(self,url,headers=False):
		self.headers = False
		self.urlp = urlparse.urlparse(url)
		return self.get_urls(url)

	def get_urls(self,url):
		return self._get_urls(url)

	def get_raw_data(self,url):
		return self._get_raw_data(url)

	def parse_data(self,data):
		return self._parse_data()

	def arrange_urls(self,urls_array):
		return self._arrange_urls(urls_array)

	def check_and_add_url(self,url):

		if(not (urlparse.urlparse(url)).hostname):
			return self.urlp.scheme+"://"+self.urlp.hostname+url
		else:
			url

	def _arrange_urls(self,urls_array):
		return urls_array

	def _parse_data(self):
		return []

	def _get_urls(self,url):
		data = self.get_raw_data(url)
		if(len(data)<=1):
			return []
		urls_array = self.parse_data(data)
		urls = self.arrange_urls(urls_array)
		return urls

	def _get_raw_data(self,url):
		try:
			if self.headers:
				request = urllib2.Request(url,headers=HEADERS_WALL_SITE)
				return (urllib2.urlopen(request)).read()
			else:
				return (urllib2.urlopen(url)).read()
		except Exception, e:
			print "======= FAILED ON ",url," ======== ",str(e)
			return ""