from bs4 import BeautifulSoup as bs
import urllib2
import urllib
import thread
import time
import os

HEADERS = {
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Encoding":"gzip,deflate,sdch",
"Accept-Language":"en-US,en;q=0.8,te;q=0.6",
"Cache-Control":"max-age=0",
"Connection":"keep-alive",
"User-Agent":"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",
}

HEADERS_WALL_SITE = {
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Encoding":"gzip,deflate,sdch",
"Accept-Language":"en-US,en;q=0.8,te;q=0.6",
"Cache-Control":"max-age=0",
"Connection":"keep-alive",
"User-Agent":"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",	
}
failed_pages = []
failed_image_page_urls = []
def get_image_url(image_page_url):
	global failed_image_page_urls
	try:
		failed_image_page_urls.remove(image_page_url)
	except Exception, e:
		pass
	try:
		request = urllib2.Request(image_page_url,headers=HEADERS_WALL_SITE)
		f = urllib2.urlopen(request)
		data = f.read()
		x = bs(data)
		x = x.prettify().encode('utf-8')
		x = bs(x)
		try:
			x = x.find_all('img',attrs={'id':'wallpaper_image'})
			x = bs(str(x[0]))
			print "image url in thread ",x.img['src']
			img_url = x.img['src']
			img_name = img_url.split('/')[-1]
			#urllib.urlretrieve(img_url,img_name)
			resource = urllib.urlopen(img_url)
			output = open(img_name,'wb')
			output.write(resource.read())
			output.close()
			try:
				failed_image_page_urls.remove(image_page_url)
			except Exception, e:
				pass
			f = open(img_name)
			lines = f.readlines()
			for counter,line in enumerate(lines):
				if "Forbidden" in line:
					print "failed . got Forbidden on line ",counter," adding url now ",image_page_url
					failed_image_page_urls.append(image_page_url)
					failed_image_page_urls = list(set(failed_image_page_urls))
					try:
						os.remove(img_name)
					except Exception, e:
						pass
					break;
				if(counter>=15):
					break;
		except Exception, e:
			print "failed at getting image url @ ",image_page_url,"error",str(e)
			failed_image_page_urls.append(image_page_url)
	except Exception, e:
		print "failed @ ",image_page_url,str(e)
		failed_image_page_urls.append(image_page_url)
	failed_image_page_urls = list(set(failed_image_page_urls))
	

def get_image_pages_urls(url):
	global failed_pages
	try:
		request = urllib2.Request(url,headers=HEADERS)
		f=urllib2.urlopen(request)
		data = f.read()
		x = bs(data)
		x = x.prettify().encode('utf-8')
		x = bs(x)
		try:
			att = x.find_all('ul',attrs={'class':'resolutionListing'})
			for each_att in att:
				try:
					x = each_att.find_all('a')
					x = x[-1]
					x = bs(str(x))
					x = x.a['href']
					print x
					thread.start_new(get_image_url,(x,))
					try:
						failed_pages.remove(url)
					except Exception, e:
						pass
				except Exception, e:
					print "failed at each_att @ ",each_att,str(e)
		except Exception, e:
			print "failed on something @ page ",i,str(e)
			failed_pages.append(url)
	except Exception, e:
		print "failed @ ",url,str(e)
		failed_pages.append(url)

def get_failed():
	print "-----------getting failed ones -----------"
	print "failed_pages",len(failed_pages)
	print "failed_image_page_urls",len(failed_image_page_urls)

	while(len(failed_image_page_urls) != 0):
		for each_url in failed_image_page_urls:
			get_image_url(each_url)

	while(len(failed_pages) != 0):
		for each_url in failed_pages:
			get_image_pages_urls(each_url)


def main_internal(url_main,pages):
	pages = pages+1
	for page_no in range(1,pages):
		url = "%spage/%s/"%(url_main,str(page_no))
		thread.start_new(get_image_pages_urls,(url,))
	while True:
		time.sleep(5)
		thread.start_new(get_failed,())

	
	

main_internal("http://www.skins.be/mila-kunis/wallpapers/",3)