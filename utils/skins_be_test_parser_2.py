from bs4 import BeautifulSoup as bs
import urllib2
import urllib
import thread
import time
import os
from utils.Engine import *
HEADERS = {
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Encoding":"gzip,deflate,sdch",
"Accept-Language":"en-US,en;q=0.8,te;q=0.6",
"Cache-Control":"max-age=0",
"Connection":"keep-alive",
"User-Agent":"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",
}

HEADERS_WALL_SITE = {
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Encoding":"gzip,deflate,sdch",
"Accept-Language":"en-US,en;q=0.8,te;q=0.6",
"Cache-Control":"max-age=0",
"Connection":"keep-alive",
"User-Agent":"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",	
}
failed_pages = []
failed_image_page_urls = []
SPACE_FOR_FUNC_1 = "     "
SPACE_FOR_FUNC_0 = ""
SPACE_FOR_FUNC_2 = "          "
def get_image_url(image_page_url,name):
	global failed_image_page_urls
	global TOTAL_IMAGES_DOWNLOADED
	try:
		failed_image_page_urls.remove(image_page_url)
	except Exception, e:
		pass
	try:
		request = urllib2.Request(image_page_url,headers=HEADERS_WALL_SITE)
		f = urllib2.urlopen(request)
		data = f.read()
		x = bs(data)
		x = x.prettify().encode('utf-8')
		x = bs(x)
		try:
			x = x.find_all('img',attrs={'id':'wallpaper_image'})
			x = bs(str(x[0]))
			print SPACE_FOR_FUNC_2,"given url is ",image_page_url
			print SPACE_FOR_FUNC_2,"image url in thread ",x.img['src']
			img_url = x.img['src']
			img_name = img_url.split('/')[-1]
			#urllib.urlretrieve(img_url,img_name)
			# resource = urllib.urlopen(img_url)
			# output = open(img_name,'wb')
			# output.write(resource.read())
			# output.close()
			save_img = ImageSaveEngine()
			val  = save_img.save(img_url,name,"bofh","Hollywood Actress",context_url=img_url)
			if(val[0] == -1):
				print val
				raise Exception("failed image")
			else:
				print SPACE_FOR_FUNC_2,"saved image ",img_url,val
			try:
				TOTAL_IMAGES_DOWNLOADED = TOTAL_IMAGES_DOWNLOADED + 1
				failed_image_page_urls.remove(image_page_url)
			except Exception, e:
				pass
			# f = open(img_name)
			# lines = f.readlines()
			# for counter,line in enumerate(lines):
			# 	if "Forbidden" in line:
			# 		print SPACE_FOR_FUNC_2,"failed . got Forbidden on line ",counter," adding url now ",image_page_url
			# 		failed_image_page_urls.append(image_page_url)
			# 		failed_image_page_urls = list(set(failed_image_page_urls))
			# 		try:
			# 			os.remove(img_name)
			# 		except Exception, e:
			# 			pass
			# 		break;
			# 	if(counter>=15):
			# 		break;
		except Exception, e:
			print SPACE_FOR_FUNC_2,"failed at getting image url @ ",image_page_url,"error",str(e)
			failed_image_page_urls.append(image_page_url)
	except Exception, e:
		print SPACE_FOR_FUNC_2,"failed @ ",image_page_url,str(e)
		failed_image_page_urls.append(image_page_url)
	failed_image_page_urls = list(set(failed_image_page_urls))
	

def get_image_pages_urls(url,name):
	global failed_pages
	global TOTAL_PAGES_DOWNLOADED
	urlsx2 = []
	print SPACE_FOR_FUNC_1,"get images urls"
	try:
		request = urllib2.Request(url,headers=HEADERS)
		f=urllib2.urlopen(request)
		data = f.read()
		print SPACE_FOR_FUNC_1,"got data"		
		x = bs(data)
		x = x.prettify().encode('utf-8')
		x = bs(x)
		try:
			att = x.find_all('ul',attrs={'class':'resolutionListing'})
			for each_att in att:
				try:
					x = each_att.find_all('a')
					x = x[-1]
					x = bs(str(x))
					x = x.a['href']
					print SPACE_FOR_FUNC_1,"x is ",x
					#thread.start_new(get_image_url,(x,))
					urlsx2.append(x)
					try:
						TOTAL_PAGES_DOWNLOADED = TOTAL_PAGES_DOWNLOADED+1
						failed_pages.remove(url)
					except Exception, e:
						pass
				except Exception, e:
					print SPACE_FOR_FUNC_1,"failed at each_att @ ",each_att,str(e)
		except Exception, e:
			print SPACE_FOR_FUNC_1,"failed on something @ page ",i,str(e)
			failed_pages.append(url)
	except Exception, e:
		print SPACE_FOR_FUNC_1,"failed @ ",url,str(e)
		failed_pages.append(url)
	for each_url in urlsx2:
		print SPACE_FOR_FUNC_1,"each_url is ",each_url
		get_image_url(each_url,name)

# def get_failed():
# 	print SPACE_FOR_FUNC_0,"-----------getting failed ones -----------"
# 	print SPACE_FOR_FUNC_0,"failed_pages",len(failed_pages)
# 	print SPACE_FOR_FUNC_0,"failed_image_page_urls",len(failed_image_page_urls)

# 	while(len(failed_image_page_urls) != 0):
# 		for each_url in failed_image_page_urls:
# 			get_image_url(each_url)

# 	while(len(failed_pages) != 0):
# 		for each_url in failed_pages:
# 			get_image_pages_urls(each_url)
def print_counters():
    TOTAL_TIME_ELAPSED = int(time.time()) - START_TIME
    TOTAL_IMAGES_FAILED = len(failed_image_page_urls)
    TOTAL_PAGES_FAILED = len(failed_pages)
    print SPACE_FOR_FUNC_0,"TOTAL TIME ELAPSED     :", TOTAL_TIME_ELAPSED
    print SPACE_FOR_FUNC_0,"TOTAL IMAGES DOWNLOADED:", TOTAL_IMAGES_DOWNLOADED
    print SPACE_FOR_FUNC_0,"TOTAL PAGES DOWNLOADED :", TOTAL_PAGES_DOWNLOADED
    print SPACE_FOR_FUNC_0,"TOTAL IMAGES FAILED    :", TOTAL_IMAGES_FAILED
    print SPACE_FOR_FUNC_0,"TOTAL PAGES FAILED     :", TOTAL_PAGES_FAILED

def print_counters_main():
    while(True):
        time.sleep(5)
        print_counters()
START_TIME = ""
TOTAL_IMAGES_DOWNLOADED = 0
TOTAL_PAGES_DOWNLOADED = 0
def main_internal(url_main,pages,name):
	global START_TIME
	START_TIME = int(time.time())
	urlsx1 = []
	pages = pages+1
	thread.start_new(print_counters_main,())
	for page_no in range(1,pages):
		url = "%spage/%s/"%(url_main,str(page_no))
		#thread.start_new(get_image_pages_urls,(url,))
		urlsx1.append(url)
	for eac in urlsx1:
		print SPACE_FOR_FUNC_0,eac
		get_image_pages_urls(eac,name)
#	while True:
#		get_failed()


def main_internal_toomany(a):
	for each_a in a:
		main_internal(each_a['url'],each_a['pages'],each_a['name'])	
	

#main_internal("http://www.skins.be/avril-lavigne/wallpapers/",24,"Avril Lavigne")
