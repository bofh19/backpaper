url = ""
main_site_url = "http://movies.sulekha.com"
name_req = ""
import urllib2
from bs4 import BeautifulSoup as bs


def get_all_urls(url,murl):
    final_urls = []
    f = urllib2.urlopen(url)
    data = f.readlines()
    datax = ''
    for each_line in data:
        datax = datax+each_line
    soup = bs(datax)
    links = soup.find_all('a',attrs={'class':'wthumbs-img'})
    for each_link in links:
        dx = str(each_link)
        sx = bs(dx)
        for each in sx.find_all('a'):
            final_urls.append(murl+str(each['href']))
    return final_urls

def get_image_url(url):
    f=urllib2.urlopen(url)
    data = f.readlines()
    datax = ''
    for each_line in data:
        datax = datax + each_line
    soup = bs(datax)
    links = soup.find_all('img',attrs={'class':'wall-img'})
    final_link = ''
    try:
        lx = str(links[0])
        sx = bs(lx)
        lxs = sx.find_all('img',attrs={'class':'wall-img'})
        for each_link in lxs:
            final_link = each_link['src']
    except Exception, e:
        pass
    return final_link

from images.views import get_proper_filename
from images.views import handle_upload_url_file
from names.models import Name
from images.models import name_images

from django.contrib.auth.models import User
from django.core.files import File

def main_obj(page_num_given):
    name_instance = Name.objects.get(name=name_req)
    
    for page_num in range(1,page_num_given):
        if page_num == 1:
            urlx = url
        else:
            urlx = url+"_"+str(page_num)
        print urlx

        all_urls = get_all_urls(urlx,main_site_url)
        for each_url in all_urls:
            urlxx = get_image_url(each_url)
            new_image = name_images()
            new_image.name = name_instance
            temp_name = urlxx.split('/')
            temp_name = temp_name[-1]
            slug_filename = get_proper_filename(temp_name)
            new_image.image_adult = name_instance.categorie.categorie_adult
            new_image_user = User.objects.get(username="utils_auto_parser")
            new_image.uploaded_by = new_image_user
            new_image.source = urlxx
            new_image.context_source  = each_url
            new_image.imagefile.save(slug_filename,File(handle_upload_url_file(urlxx)))
            print "saved image : " + urlxx + " filename : "+slug_filename

def main_obj_from_wallpapers(url_given,page_num_given,name_given):
    global url
    global name_req
    url = url_given
    name_req = name_given
    main_obj(page_num_given)

# ------ 

def get_urls_picture_gallery(url,murl):
    final_urls = []
    f = urllib2.urlopen(url)
    data = f.readlines()
    datax = ''
    for each_line in data:
        datax = datax + each_line
    soup = bs(datax)
    lis = soup.find_all('ul',attrs={'class':'actor-thumbnail-listing'})
    if len(lis)<=0:
        return final_urls
    lis = lis[0]
    sx = bs(str(lis))
    links = sx.find_all('a')
    for each_link in links:
       # print "each_link",each_link
        final_urls.append(murl+str(each_link['href']))
    return final_urls

def get_image_url_picture_gallery(url):
    f = urllib2.urlopen(url)
    data = f.readlines()
    datax = ''
    for each_line in data:
        datax = datax + each_line
    soup = bs(datax)
    parent = soup.find_all('div',attrs={'class':'actor-zoom-image-display'})
    if(len(parent)<=0):
        return ''
    parent = parent[0]
    parent_bs = bs(str(parent))
    final_link =''
    ixs = parent_bs.find_all('img')
    try:
        final_link=str(ixs[0]['src'])
        return final_link
    except Exception, e:
        return final_link



def main_obj_from_picture_gallery(url_given,pagenum_given,name_given):
    name_instance = Name.objects.get(name=name_given)
    
    for page_num in reversed(range(1,pagenum_given+1)):
        if page_num == 1:
            urlx = url_given
        else:
            urlx = url_given+"_"+str(page_num)
        print urlx

        all_urls = get_urls_picture_gallery(urlx,main_site_url)
        for each_url in all_urls:
            urlxx = get_image_url_picture_gallery(each_url)
            if(urlxx != ''):
                new_image = name_images()
                new_image.name = name_instance
                temp_name = urlxx.split('/')
                temp_name = temp_name[-1]
                slug_filename = get_proper_filename(temp_name)
                new_image.image_adult = name_instance.categorie.categorie_adult
                new_image_user = User.objects.get(username="utils_auto_parser")
                new_image.uploaded_by = new_image_user
                new_image.source = urlxx
                new_image.context_source  = each_url
                new_image.imagefile.save(slug_filename,File(handle_upload_url_file(urlxx)))
                print "saved image : " + urlxx + " filename : "+slug_filename

