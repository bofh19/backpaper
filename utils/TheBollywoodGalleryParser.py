from utils.Engine import *
import urlparse

class TheBollywoodGalleryParserL1(GetImageBaseClass):
	def parse_data(self,data):
		x = bs(data)
		attrs = x.find_all('img',attrs={'class':'attachment-thumbnail'})
		urls = []
		for each_attr in attrs:
			urls.append(each_attr.parent['href'])
		attrs = x.find_all('img',attrs={'class':'aligncenter size-large wp-image-81062'})
		for each_attr in attrs:
			urls.append(each_attr.parent['href'])
		return urls

class TheBollywoodGalleryParserL2(GetImageBaseClass):
	def parse_data(self,data):
		x=bs(data)
		attrs = x.find_all('img',attrs={'class':'attachment-medium'})
		urls = []

		for each_attr in attrs:
			urls.append(each_attr.parent['href'])

		return urls

import time
failed_urls = []
failed_imgs = []

def main_internal2(url,name):
	strt_time = int(time.time())
	a=TheBollywoodGalleryParserL1()
	urls = a.execute(url)
	img_urls = []
	global failed_urls
	global failed_imgs
	
	a = ImageSaveEngine()
	print "going to save",urls
	print "Time = ",int(time.time()) - strt_time
	for each_img in urls:
		val  = a.save(each_img,name,"bofh","Bollywood",context_url=url)
		if(val[0] == -1):
			print "failed @ ",each_img,val
			failed_imgs.append(each_img)
	print "finished in ",int(time.time()) - strt_time

def main_internal(url,name,cat="Bollywood"):
	
	strt_time = int(time.time())
	a=TheBollywoodGalleryParserL1()
	urls = a.execute(url)
	img_urls = []
	global failed_urls
	global failed_imgs
	a=TheBollywoodGalleryParserL2()
	print "going to lvl 2",urls
	print "Time = ",int(time.time()) - strt_time
	for each_url in urls:
		try:
			img_urls.append(a.execute(each_url)[0])
		except Exception, e:
			failed_urls.append(each_url)
	
	a = ImageSaveEngine()
	print "going to save",img_urls
	print "Time = ",int(time.time()) - strt_time
	for each_img in img_urls:
		val  = a.save(each_img,name,"bofh",cat,context_url=url)
		if(val[0] == -1):
			print "failed @ ",each_img,val
			failed_imgs.append(each_img)
	print "finished in ",int(time.time()) - strt_time

def get_failed():
	a=TheBollywoodGalleryParserL2()
	
	global failed_urls
	global failed_imgs
	failed_urls = list(set(failed_urls))
	for each_url in failed_urls:
		try:
			img_urls.append(a.execute(each_url)[0])
			try:
				failed_urls.remove(each_url)
			except Exception, e:
				pass
		except Exception, e:
			pass

	img_urls = list(set(failed_imgs))

	for each_img in img_urls:
		val  = a.save(each_img,name,"bofh","Bollywood")
		if(val[0] == -1):
			print "failed @ ",each_img,val
		else:
			try:
				failed_imgs.remove(each_img)
			except Exception, e:
				pass