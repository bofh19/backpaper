from cv2_detect import *

from images.models import name_images
from utils.models import cvImagesDirection
import time

def align_direction_all():
	left_out_images = name_images.objects.exclude(id__in=cvImagesDirection.objects.all().values_list('image_id'))[::-1]
	start = time.time()
	changed_counter = 0
	for counter,c_img in enumerate(left_out_images):
		c_direction = detect(c_img)[1]
		c_img.crop_direction = c_direction
		c_img.save()

		new_direction = cvImagesDirection()
		new_direction.image_id = c_img
		new_direction.crop_direction = c_direction

		new_direction.save() 
		end = time.time()
		if(c_direction != 0):
			changed_counter = changed_counter+1
		print " - --- --- ---- --- "
		print "finished ",counter," images left ",len(left_out_images)-counter," image id->",c_img.id," elapsed time ->",str((end-start)/60)," changed this session ->",changed_counter