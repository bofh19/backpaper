# cv2_detect
import cv2
import cv2.cv as cv
import numpy
from images.models import name_images


def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)

def detect(in_fn, cascade_fn='/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml',
           scaleFactor=1.3, minNeighbors=4, minSize=(20, 20),
           flags=cv.CV_HAAR_SCALE_IMAGE):
    direction = 0
    try:
        print ">>> Loading image..."
        if(str(type(in_fn)).find('models')):
            in_fn = in_fn.imagefile.path
            print in_fn
        img_color = cv2.imread(in_fn)
        img_gray = cv2.cvtColor(img_color, cv.CV_RGB2GRAY)
        img_gray = cv2.equalizeHist(img_gray)
        img = img_gray
        print ">>> finding "
        cascade = cv2.CascadeClassifier(cascade_fn)
        rects = cascade.detectMultiScale(img, scaleFactor=scaleFactor,
                                         minNeighbors=minNeighbors,
                                         minSize=minSize, flags=flags)
        if len(rects) == 0:
            print "nothing found"
            return ([],direction)
        rects[:, 2:] += rects[:, :2]
        #print rects
        #print "xs might be ",rects[0][0],rects[0][2]
        #print "ys might be ",rects[0][1],rects[0][3]
        centerx = (rects[0][0]+rects[0][2])/2
        centery = (rects[0][1]+rects[0][3])/2
        print "found something ",rects
        #print "center ",centerx,centery
        #print "img sizes",img.shape
        #print "img_center ",img.shape[0]/2,img.shape[1]/2
        img_x = img.shape[1]
        if(centerx>img_x/3):
            if(centerx>(img_x/3)*2):
                print "right"
                direction = name_images.right
            else:
                print "center"
                direction = name_images.center
        else:
            print "left"
            direction = name_images.left
        #print "writing test out"
        #rects2 = numpy.array([[0,0,1920,1200]])
        # img_out = img_color.copy()
        # draw_rects(img_out, rects, (0, 255, 0))
        # cv2.imwrite("testout.jpg", img_out)
        return (rects,direction)
    except Exception, e:
        print e
        return ([],0)
    