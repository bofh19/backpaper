from images.models import name_images
from sorl.thumbnail import get_thumbnail


def pre_crop_screen(screen_size):
    #screen_size = '1080x1920' #widthxheight
    images_all = name_images.objects.all()
    images_cropped = []
    total_count = len(images_all)
    for counter, each_image in enumerate(images_all):
        try:
            images_cropped.append(get_thumbnail(each_image.imagefile, screen_size, crop='center'))
        except Exception, e:
            print e
        print total_count, counter, each_image.imagefile


def pre_crop_all():
    screen_size = ['1080x1920', '720x1280', '800x1280', '540x960', '320x480', '480x800', '600x976', '200x320',
                   '540x240', '320x160', '240x120', '720x1184', '100x160', '150x240',
                   '300x480', '768x1280', '768x1184', '768x1212', '540x888'] #widthxheight
    screen_sizes = list(set(screen_size))
    images_all = name_images.objects.all()
    total_count = len(images_all)
    for each_screen in screen_sizes:
        images_cropped = []
        for counter, each_image in enumerate(images_all):
            try:
                images_cropped.append(get_thumbnail(each_image.imagefile, each_screen, crop='center'))
            except Exception, e:
                print e
            print each_screen, total_count, counter, each_image.imagefile


def pre_crop_all_db():
    from users.models import DevResolutions
    all_resolutions = DevResolutions.objects.all()
    total_reso_counter = len(all_resolutions)
    reso_counter = 0
    images_all = name_images.objects.all()
    total_count = len(images_all)
    for each_screen in all_resolutions:
        reso_counter = reso_counter + 1
        images_cropped = []
        for counter, each_image in enumerate(images_all):
            try:
                images_cropped.append(get_thumbnail(each_image.imagefile, each_screen.get_resolution(), crop='center'))
            except Exception, e:
                print e
            print total_reso_counter,reso_counter,each_screen.get_resolution(), total_count, counter, each_image.imagefile