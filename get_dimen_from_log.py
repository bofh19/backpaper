import re

def getall(file_path):
    f=open(file_path)
    width_arr=set()
    height_arr=set()
    global dimen
    dimen={}
    t_dimen = {}
    for line in f.readlines():
        if(re.search(r'Android',line)):
            width = get_number(line,'device_width')
            height = get_number(line,'device_height')
            t_width = get_number(line,'width')
            t_height = get_number(line,'height')
            try:
                if width:
                    if height:
                        dimen[width] = list(set(dimen[width].add(height)))
            except Exception, e:
                if width:
                    if height:
                        dimen[width] = list(set([height]))
            try:
                if t_width:
                    if t_height:
                        t_dimen[t_width] = list(set(t_dimen[t_width].add(t_height)))
            except Exception, e:
                if t_width:
                    if t_height:
                        t_dimen[t_width] = list(set([t_height]))
    print dimen
    print t_dimen
    return {'dimen':dimen,'t_dimen':t_dimen}
dimen = {}
def get_number(line,attr):
    if(re.search(attr+'=\d+',line)):
       attr_value = re.search(attr+'=\d+',line).group()
       attr_value = re.split('=',attr_value)[1]
       return attr_value
    else:
        return None



import sys

def get_from_latest_log():
    intodb('/home/praveenr/logs/user/access_backpaper.log')

def intodb(filepath):
    from users.models import DevResolutions
    array = getall(filepath)
    for width,heights in array['dimen'].iteritems():
        for height in heights:
            new_reso = DevResolutions()
            new_reso.width = width
            new_reso.height = height
            try:
                new_reso.save()
                print "Saved",width,"x",height
            except Exception:
                pass
    for width,heights in array['t_dimen'].iteritems():
        for height in heights:
            new_reso = DevResolutions()
            new_reso.width = width
            new_reso.height = height
            try:
                new_reso.save()
                print "Saved",width,"x",height
            except Exception:
                pass




if __name__=='__main__':
    getall(sys.argv[1])


"""
'111.93.21.110 - - [27/May/2013:09:04:50 +0000] "GET /api/v1/name_images/?name=13&format=json&device_width=480&height=240&width=150&limit=20&offset=20&device_height=800 HTTP/1.0" 200 5133 "-" "Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-I9082 Build/JZO54K)"'

'111.93.21.110 - - [27/May/2013:09:05:49 +0000] "GET /admin/jsi18n/ HTTP/1.0" 200 5169 "http://backpaper.w4rlock.in/admin/images/name_images/459/" "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36"'
"""

