
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse

from images.models import name_images
from names.models import Name
from categories.models import Categories

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import slugify

def multi_file_upload(request):
	if request.user.is_staff:
		pass
	else:
		return HttpResponseRedirect(str(reverse('django.contrib.auth.views.login')) + '?next='+request.path)
	if request.method == 'POST':
		name_instance = get_name_instance(request)
		if(name_instance == -1):
			return HttpResponse("Error - Check Server logs")
		
		for f in request.FILES.getlist('docfile'):
			new_image = name_images()
			new_image.name = name_instance
			slug_filename = get_proper_filename(f.name)
			new_image.image_adult = name_instance.categorie.categorie_adult
			new_image.uploaded_by = request.user
			new_image.imagefile.save(slug_filename,File(handle_upload_file(f)))
			
	return render_to_response(
				'images/templates/multi_upload.html',
				{},context_instance=RequestContext(request)
					)

def handle_upload_file(f):
	img_temp = NamedTemporaryFile()
	for chunk in f.chunks():
		img_temp.write(chunk)
	img_temp.flush()
	return img_temp


import urlparse
import re
def multi_url_upload(request):
	if request.user.is_staff:
		pass
	else:
		return HttpResponseRedirect(str(reverse('django.contrib.auth.views.login')) + '?next='+request.path)
	message = '<br/>'
	urls_array = []
	if request.method == 'POST':
		name_instance = get_name_instance(request)
		if(name_instance == -1):
			message += "Error Check Server Logs <br/>"
			
		for url in request.POST.getlist('doctext'):
			try:
				handle_upload_url_file(url)
				new_image = name_images()
				new_image.name = name_instance
				new_image.source = url
				filename = url.split('/')[-1]
				slug_filename = get_proper_filename(filename)
				new_image.image_adult = name_instance.categorie.categorie_adult
				new_image.uploaded_by = request.user
				new_image.imagefile.save(slug_filename,File(handle_upload_url_file(url)))
				message = message + "saved on url "+url+"<br/>"
			except Exception, e:
				a=urlparse.urlparse(url)
				if( a.scheme == 'http'):
					print 'url ',url
					urls_array.append(url)
				# message = message + str(e.message) + "<br/>"
				message = message + str(e) + "<br/>"
	return render_to_response(
				'images/templates/multi_url_upload.html',
				{
				'message':message,
				'urls_array':urls_array
				},context_instance=RequestContext(request)
					)

def get_proper_filename(filename):
	ax = filename.split('.')
	if len(ax) >=2 :
		file_ext = '.'+filename.split('.')[-1]
		slug_filename = slugify(filename.replace(file_ext,''))+file_ext
	else:
		slug_filename = slugify(filename)
	slug_filename = slug_filename.replace('-','_')
	return slug_filename
#	import string
#	valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
#	return ''.join(c for c in filename if c in valid_chars)
	
import urllib2
def handle_upload_url_file(url):
	img_temp = NamedTemporaryFile()
	opener = urllib2.build_opener()
	opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]
	img_temp.write(opener.open(url).read())
	img_temp.flush()
	return img_temp


def get_name_instance(request):
	name_req = request.POST.get('name','')
	if (name_req == '' or name_req == ' '):
		print "no name_req given for new name"
		message = "no name_req given for new name"
		return -1
		# return -1
	try:
		name_instance = Name.objects.get(name=name_req)
	except ObjectDoesNotExist, e:
		cat_req = request.POST.get('categorie','')
		if (cat_req == '' or cat_req == ' '):
			print "no catagorie given for new name"
			return -1
		try:
			cat_instance = Categories.objects.get(categorie_name = cat_req)
			print "got cat "
			print cat_instance
		except ObjectDoesNotExist, e:
			new_cat_instance = Categories()
			new_cat_instance.categorie_name = cat_req
			new_cat_instance.created_by = 'A'
			new_cat_instance.save()
			cat_instance = new_cat_instance
		except Exception,e:
			print "something went wrong seriously while getting cat_instance"
			print e
			return -1

		new_name_instance = Name()
		new_name_instance.categorie = cat_instance
		new_name_instance.name = name_req
		new_name_instance.created_by = 'A'
		new_name_instance.save()
		name_instance = new_name_instance
		
	except Exception, e:
		print "something went wrong seriously  while getting name_instance"
		print e
		return -1

	return name_instance
