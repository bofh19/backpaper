
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse

from images.models import name_images
from names.models import Name
from categories.models import Categories

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import slugify

import uuid
import base64

from images.views.multi_upload import get_name_instance
from images.views.multi_upload import get_proper_filename

def multi_file_upload2(request):
	if request.user.is_staff:
		pass
	else:
		return HttpResponseRedirect(str(reverse('django.contrib.auth.views.login')) + '?next='+request.path)
	if request.method == 'POST':
		name_instance = get_name_instance(request)
		if(name_instance == -1):
			print "ERROR"
			return HttpResponse("Error - Check Server logs "+" Name Instance value -1 "+request.POST.get('file_name',''))
		try:
			new_image = name_images()
			new_image.name = name_instance
			slug_filename = get_proper_filename(request.POST.get('file_name',str(uuid.uuid4())))
			new_image.image_adult = name_instance.categorie.categorie_adult
			new_image.uploaded_by = request.user
			new_image.imagefile.save(slug_filename,File(handle_upload_data(request.POST.get('value'))))
			return HttpResponse("Saved Sucessfully "+slug_filename)
		except Exception, e:
			return HttpResponse("Error while saving Python Error "+str(e))
		
	return render_to_response(
				'images/templates/multi_file_ajax_upload.html',
				{},context_instance=RequestContext(request)
					)



def handle_upload_data(data):
	img_temp = NamedTemporaryFile()
	data = data.split(',')
	encoded_data = data[1].replace(' ',',')
	img_temp.write(base64.b64decode(encoded_data))
	img_temp.flush()
	return img_temp