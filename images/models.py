from django.db import models

# Create your models here.

import os
import uuid
import datetime

from names.models import Name
from categories.models import Tags

from django.contrib.auth.models import User


def get_file_path(instance, filename):
    return os.path.join(instance.directory_string_var, filename)


class name_images(models.Model):
    date = models.DateField(auto_now=True)
    name = models.ForeignKey(Name, related_name='name_fk')
    imagefile = models.ImageField(upload_to=get_file_path)

    now = datetime.datetime.now()
    directory_string_var = 'image/%s/%s/%s/' % (now.year, now.month, now.day)
    image_enabled = models.BooleanField(default=True)

    source = models.CharField(max_length=250, blank=True, null=True)
    context_source = models.CharField(max_length=250,blank=True,null=True)
    image_adult = models.BooleanField(default=False)

    right = 1
    center = 0
    left = -1
    CROP_SIDE_CHOICES = (
    (right, u'Right'),
    (center, u'Center'),
    (left, u'Left'),
    )
    crop_direction = models.IntegerField(verbose_name=("Crop Direction"), choices=CROP_SIDE_CHOICES,default=center)
    ranks_used = models.IntegerField(default=0)
    uploaded_by = models.ForeignKey(User)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True,blank=True,null=True) 
    def __unicode__(self):
        return self.name.name

    class Meta:
        ordering = ('-ranks_used','-id')


class tags_images(models.Model):
    tag = models.ForeignKey(Tags)
    image = models.ForeignKey(name_images)

    def __unicode__(self):
        return self.tag.tag_name + " - " + self.image.name.name

