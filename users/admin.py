
from django.contrib import admin
from users.models import user_fav
from users.models import DeviceAccessors,DeviceNotifications,NotificationsSent

admin.site.register(user_fav)
admin.site.register(DeviceAccessors)
admin.site.register(DeviceNotifications)
admin.site.register(NotificationsSent)

from users.models import DevResolutions
admin.site.register(DevResolutions)