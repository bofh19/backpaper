from django.db import models

# Create your models here.

from django.contrib.auth.models import User

from names.models import Name
from images.models import name_images


class user_fav(models.Model):
    user_id = models.ForeignKey(User)
    name_id = models.ForeignKey(Name)

    class Meta:
        unique_together = ('user_id', 'name_id')

    def __unicode__(self):
        output = self.user_id.username + ' - ' + self.name_id.name
        return output


class DeviceAccessors(models.Model):
    dev_id = models.CharField(max_length=255, verbose_name=("DeviceId"))
    img_id = models.ForeignKey(name_images, verbose_name=("ImageId"))
    LWP = 1
    Share = 2
    Download = 3
    Wallpaper = 4
    Fav = 5
    TYPE_CHOICES = (
    (LWP, u'Live Wallpaper'),
    (Share, u'Sharing'),
    (Download, u'Download'),
    (Wallpaper, u'Wallpaper'),
    (Fav,u'Favorite'),
    )
    type_taken = models.IntegerField(verbose_name=("Type Taken"), choices=TYPE_CHOICES)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True)

    class Meta:
        unique_together = ('dev_id', 'img_id', 'type_taken')

    def clean(self):
        # print "crap"
        dev_id_cleaned = self.dev_id
        if (re.search(' ', dev_id_cleaned)):
            raise ValidationError("Invalid Dev ID")
        super(DeviceAccessors, self).clean()

    def save(self):
        # print "crap save"
        dev_id_cleaned = self.dev_id
        if (re.search(' ', dev_id_cleaned)):
            raise ValidationError("Invalid Dev ID")
        type_taken_cleaned = self.type_taken
        if (int(type_taken_cleaned) > len(self.TYPE_CHOICES)):
            raise ValidationError("Invalid type_taken")
        super(DeviceAccessors, self).save()


from django.core.exceptions import ValidationError
import re


class DeviceNotifications(models.Model):
    dev_id = models.CharField(max_length=255, verbose_name=("DeviceId"), unique=True, primary_key=True)
    reg_id = models.TextField(verbose_name=("RegID"), blank=True, null=True)
    is_active = models.BooleanField(verbose_name=("Is active?"), default=False)
    user_id = models.ForeignKey(User, null=True, blank=True)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name=("Modified date"), auto_now=True)

    class Meta:
        verbose_name = ("Device Notifications")
        verbose_name_plural = ("Device Notifications")
        ordering = ['-modified_date']

    def clean(self):
        # print "crap"
        dev_id_cleaned = self.dev_id
        if (re.search(' ', dev_id_cleaned)):
            raise ValidationError("Invalid Dev ID")
        super(DeviceNotifications, self).clean()

    def save(self):
        # print "crap save"
        dev_id_cleaned = self.dev_id
        if (re.search(' ', dev_id_cleaned)):
            raise ValidationError("Invalid Dev ID")
        super(DeviceNotifications, self).save()


class NotificationsSent(models.Model):
    dev_id = models.CharField(max_length=255, verbose_name=("DeviceId"))
    gcm_response = models.CharField(max_length=255, verbose_name=("GCMResponse"))
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name=("Modified date"), auto_now=True)


class DevRatings(models.Model):
    dev_id = models.CharField(max_length=255, verbose_name=("DeviceId"))
    img_id = models.ForeignKey(name_images, verbose_name=("ImageId"))
    RATING_CHOICES = (
    (u'-1', u'-1'),
    (u'0', u'0'),
    (u'1', u'1'),
    )
    rating = models.IntegerField(max_length=1, default=0, choices=RATING_CHOICES)

    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name=("Modified date"), auto_now=True)

    class Meta:
        unique_together = ('dev_id', 'img_id')


class DevResolutions(models.Model):
    width = models.IntegerField()
    height = models.IntegerField()
    last_cropped_image = models.IntegerField(default=0)
    def __unicode__(self):
        return str(self.width)+'x'+str(self.height)

    def get_resolution(self):
        return str(self.width)+'x'+str(self.height)

    class Meta:
        unique_together = ('width','height')