# Create your views here.
from users.models import DeviceAccessors
from images.models import name_images

import json
from django.http import HttpResponse

mimetype = 'application/json'


def devAcc(request):
    result = {}
    try:
        dev_id = request.GET['dev_id']
        result['dev_id'] = dev_id
        img_id = request.GET['img_id']
        result['img_id'] = img_id
        type_taken = request.GET['type_taken']
        result['type_taken'] = type_taken
    except Exception, e:
        result['status'] = 'failed'
        result['exception'] = str(e)
        result_json = json.dumps(result)
        return HttpResponse(result_json, mimetype)

    try:
        given_image = name_images.objects.get(id=img_id)
    except Exception, e:
        result['status'] = 'failed'
        result['exception'] = str(e)
        result_json = json.dumps(result)
        return HttpResponse(result_json, mimetype)
    try:
        new_devAcc = DeviceAccessors()
        new_devAcc.dev_id = dev_id
        new_devAcc.img_id = given_image
        new_devAcc.type_taken = type_taken
        new_devAcc.save()
    except Exception, e:
        result['status'] = 'failed'
        result['exception'] = str(e)
        result_json = json.dumps(result)
        return HttpResponse(result_json, mimetype)

    result['status'] = 'sucess'
    result_json = json.dumps(result)
    return HttpResponse(result_json, mimetype)

