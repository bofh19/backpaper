__author__ = 'w4rlock'
from images.models import name_images
from sorl.thumbnail import get_thumbnail

from django.core.management.base import BaseCommand, CommandError
class Command(BaseCommand):
    args = ''
    help = 'Pre Crop All Images for resolutions in database'

    def handle(self, *args, **options):
        self.pre_crop_all_db()

    def pre_crop_all_db(self):
        from users.models import DevResolutions
        all_resolutions = DevResolutions.objects.all()
        total_reso_counter = len(all_resolutions)
        reso_counter = 0
        images_all = name_images.objects.all().order_by('-ranks_used')
        total_count = len(images_all)
        for each_screen in all_resolutions:
            reso_counter += 1
            images_cropped = []
            this_screen_images = images_all.filter(id__gt = each_screen.last_cropped_image)
            last_image = ''
            for counter, each_image in enumerate(this_screen_images):
                try:
                    images_cropped.append(get_thumbnail(each_image.imagefile, each_screen.get_resolution(), crop='center'))
                except Exception, e:
                    print e
                    self.stdout.write(str(e))
                # print total_reso_counter,reso_counter,each_screen.get_resolution(), total_count, counter, each_image.imagefile
                last_image = each_image
                self.stdout.write(str(total_reso_counter)+" "+str(reso_counter)+" "+str(each_screen.get_resolution())+" "+str(total_count)+" "+str(counter)+" "+str(each_image.id)+" "+str(each_image.imagefile))
            try:
                each_screen.last_cropped_image = last_image.id
                each_screen.save()
            except Exception, e:
                self.stdout.write(str(e))