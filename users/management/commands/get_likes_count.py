__author__ = 'w4rlock'

from sorl.thumbnail import get_thumbnail
from names.models import Name
from images.models import name_images
from scomments.models import sLikes
from django.db.models import Sum as AnnotateSum

from django.core.management.base import BaseCommand, CommandError
class Command(BaseCommand):
    args = ''
    help = 'Get likes Count for All Names'

    def handle(self, *args, **options):
        all_names = Name.objects.all()
        for each_name in all_names:
            
            unique_group_id = 'A_'+str(each_name.id)
            try:
                objects = sLikes.objects.filter(unique_group_id=unique_group_id).aggregate(ranks_sum=AnnotateSum('rank_given'))
                if(objects['ranks_sum'] is None):
                    objects['ranks_sum'] = 0
                objects['unique_group_id'] = unique_group_id
                objects['total_no_ranks'] = sLikes.objects.filter(unique_group_id=unique_group_id).count()
                objects['likes_votes_count'] = sLikes.objects.filter(unique_group_id=unique_group_id,rank_given=1).count();
                objects['dislikes_votes_count'] = sLikes.objects.filter(unique_group_id=unique_group_id,rank_given=-1).count();
                each_name.total_votes_count = objects['total_no_ranks']
                each_name.sum_votes_count = objects['ranks_sum']
                each_name.likes_votes_count = objects['likes_votes_count']
                each_name.dislikes_votes_count = objects['dislikes_votes_count']
                each_name.save()
                self.stdout.write(each_name.name+" "+"total_no_ranks: "+str(objects['total_no_ranks'])+" "+"ranks_sum: "+str(objects['ranks_sum']) + " likes_votes_count: "+str(objects['likes_votes_count'])+" dislikes_votes_count: "+str(objects['dislikes_votes_count']))
            except Exception, e:
                print e