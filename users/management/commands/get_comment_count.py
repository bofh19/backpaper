__author__ = 'w4rlock'

from sorl.thumbnail import get_thumbnail
from names.models import Name
from images.models import name_images
from scomments.models import sComments

from django.core.management.base import BaseCommand, CommandError
class Command(BaseCommand):
    args = ''
    help = 'Get Comment Count for All Names'

    def handle(self, *args, **options):
        all_name_comments = sComments.objects.filter(unique_group_id__contains='A_')
        a_all = {}
        for each_c in all_name_comments:
            to_id = each_c.unique_group_id
            a_id = to_id.split('_')[1]
            a_id = int(a_id)
            try:
                a_all[a_id] = a_all[a_id]  + 1
            except Exception, e:
                a_all[a_id] = 1
        all_image_comments = sComments.objects.filter(unique_group_id__contains='I_')
        for each_c in all_image_comments:
            to_id = each_c.unique_group_id
            i_id = to_id.split('_')[1]
            a_id = name_images.objects.get(id = i_id)
            a_id = a_id.name.id
            a_id = int(a_id)
            try:
                a_all[a_id] = a_all[a_id]  + 1
            except Exception, e:
                a_all[a_id] = 1
        for a_id,c_count in a_all.items():
	    try:
                current_name = Name.objects.get(id=a_id)
                current_name.comments_count = c_count
                self.stdout.write(str(current_name.name)+" "+str(c_count))
                current_name.save()
	    except Exception, e:
		pass
