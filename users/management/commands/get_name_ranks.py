__author__ = 'w4rlock'

import re
import operator

from django.core.management.base import BaseCommand, CommandError
class Command(BaseCommand):
    args = ''
    help = 'Get NAMES Used <like downloaded/set as wallpaper/shared etc.. / Ranks From Images'

    def handle(self, *args, **options):
        self.stdout.write("Ranks Based on Used <like downloaded/set as wallpaper/shared etc..>")
        self.get_ranks2()

    def get_ranks(self):
        # f= open('/home/praveenr/backpaper_logs/access_backpaper.log')
        img_id_all = {}
        all_lines = []
        for line in f.readlines():
            all_lines.append(line)
        f.close()
        for i in range(1,8):
            f=open('/home/praveenr/backpaper_logs/access_backpaper.log.'+str(i))
            for line in f.readlines():
                all_lines.append(line)
            f.close()
        #import os
        #for dirname,dirnames,filenames in os.walk('/home/praveenr/backpaper_logs/'):
        #    for filename in filenames:
        #        f=open(os.path.join(dirname,filename))
        #        for line in f.readlines():
        #            all_lines.append(line)
        #        f.close()
        for line in all_lines:
            if(re.search('type_taken',line)):
                img_id = re.search('img_id'+'=\d+',line).group()
                img_id = re.split('=',img_id)[1]
                try:
                    img_id_all[img_id] = img_id_all[img_id]+1
                except Exception, e:
                    img_id_all[img_id] = 1
        img_sorted = sorted(img_id_all.iteritems(),key=operator.itemgetter(1))
        img_sorted.reverse()
        names_ranks = {}
        from names.models import Name
        from images.models import name_images
        all_names = Name.objects.all()
        all_images = name_images.objects.all()
        for each_image in img_sorted:
            try:
                names_ranks[all_names.get(id=(all_images.get(id=each_image[0])).name_id).name] += each_image[1]
            except Exception,e:
                names_ranks[all_names.get(id=(all_images.get(id=each_image[0])).name_id).name] = each_image[1]
        #print img_sorted
        names_ranks_sorted = sorted(names_ranks.iteritems(),key=operator.itemgetter(1))
        names_ranks_sorted.reverse()
        self.stdout.write(str(names_ranks_sorted))

    def get_ranks2(self):
        img_id_all = {}
        #import os
	loc = '/home/praveenr/logs/user/access_backpaper.log'
        loc2 = loc+'.'
        all_filenames = [loc,loc2+str(1),loc2+str(2),loc2+str(3),loc2+str(4),loc2+str(5),loc2+str(6),loc2+str(7)]
#        for dirname,dirnames,filenames in os.walk('/home/praveenr/backpaper_logs/'):
        for filename in all_filenames:
#            f=open(os.path.join(dirname,filename))
	    self.stdout.write(filename)
	    f=open(filename)
            for line in f.readlines():
                if(re.search('type_taken',line)):
                    img_id = re.search('img_id'+'=\d+',line).group()
                    img_id = re.split('=',img_id)[1]
                    try:
                        img_id_all[img_id] = img_id_all[img_id]+1
                    except Exception, e:
                        img_id_all[img_id] = 1
            f.close()
        img_sorted = sorted(img_id_all.iteritems(),key=operator.itemgetter(1))
        img_sorted.reverse()
        names_ranks = {}
        from names.models import Name
        from images.models import name_images
        all_names = Name.objects.all()
        all_images = name_images.objects.all()
        for each_image in img_sorted:
            try:
                updating_image = all_images.get(id=each_image[0])
                updating_image.ranks_used = each_image[1]
                updating_image.save()
            except Exception,e:
                self.stdout.write(str(e))
            try:
                names_ranks[all_names.get(id=(all_images.get(id=each_image[0])).name_id).name] += each_image[1]
            except Exception,e:
                names_ranks[all_names.get(id=(all_images.get(id=each_image[0])).name_id).name] = each_image[1]
        #print img_sorted
        names_ranks_sorted = sorted(names_ranks.iteritems(),key=operator.itemgetter(1))
        names_ranks_sorted.reverse()
        for each_name in names_ranks_sorted:
            try:
                updating_name = all_names.get(name=each_name[0])
                updating_name.ranks_used = each_name[1]
                updating_name.save()
            except Exception,e:
                self.stdout.write(str(e))
        self.stdout.write(str(names_ranks_sorted))

