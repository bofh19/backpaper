from django.core.management.base import BaseCommand, CommandError
import urllib
import urllib2

API_KEY = "AIzaSyC4RVO92rxTMaV0EwB5QY2iBiY-iZuxO6U"
from users.models import *
from images.models import *
import datetime


class Command(BaseCommand):
    args = ''
    help = 'Sends Notification to all Activated Phones'

    def handle(self, *args, **options):
        all_enabled_phones = DeviceNotifications.objects.filter(is_active=True)
	#all_enabled_phones = DeviceNotifications.objects.filter(dev_id='5d69af7a37d9a967')
	#self.stdout.write(str(all_enabled_phones))
        today_date = datetime.datetime.now()
        yesterday_date = today_date - datetime.timedelta(days=1)
        latest_images = name_images.objects.filter(creation_date__range=[yesterday_date, today_date],
                                                   image_adult=False).order_by('-id')
        #latest_images = name_images.objects.filter(date=datetime.datetime.now()).order_by('-id')[:10]
        api_key = API_KEY
	datax = {'title': 'App Update', 'message': 'As Google has suspended the App / we have created a new App with the same name. Please Install latest version / 20000 Indian Heroine Wallpaper / to get futher Updates','actors': str('As Google has suspended the App / we have created a new App with the same name. Please Install latest version / 20000 Indian Heroine Wallpaper / to get futher Updates')}
	self.stdout.write(str(datax))
        for each_phone in all_enabled_phones:
            phone_key = each_phone.reg_id
            values = {'registration_id':phone_key}
            for k, v in datax.items():
                values["data.%s" % k] = v.encode('utf-8')
            data = urllib.urlencode(values)
            headers = {
            'UserAgent': "GCM-Server",
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
            'Authorization': 'key=' + api_key,
            'Content-Length': str(len(data))
            }
            request = urllib2.Request("https://android.googleapis.com/gcm/send", data, headers)

            response = urllib2.urlopen(request)
            result = response.read()
            #self.stdout.write(data)
            try:
                new_notification_sent = NotificationsSent()
                new_notification_sent.dev_id = each_phone.dev_id
                new_notification_sent.gcm_response = result
                new_notification_sent.save()
            except Exception, e:
                self.stdout.write(str(e))

            self.stdout.write(result)
