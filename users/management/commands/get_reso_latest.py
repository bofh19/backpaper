__author__ = 'w4rlock'

from django.core.management.base import BaseCommand, CommandError
import re

class Command(BaseCommand):
    args = ''
    help = 'get resolutions from latest log file and insert into db'

    def handle(self, *args, **options):
        #self.intodb('/home/praveenr/backpaper_logs/access_backpaper.log')
	self.intodb('/home/praveenr/logs/user/access_backpaper.log')
        # self.intodb('C:\\Users\\w4rlock\\Desktop\\crap\\access_backpaper.log')

    def getall(self,file_path):
        f=open(file_path)
        width_arr=set()
        height_arr=set()
        global dimen
        dimen={}
        t_dimen = {}
        for line in f.readlines():
            if(re.search(r'Android',line)):
                width = self.get_number(line,'device_width')
                height = self.get_number(line,'device_height')
                t_width = self.get_number(line,'width')
                t_height = self.get_number(line,'height')
                try:
                    if width:
                        if height:
                            dimen[width] = list(set(dimen[width].add(height)))
                except Exception, e:
                    if width:
                        if height:
                            dimen[width] = list(set([height]))
                try:
                    if t_width:
                        if t_height:
                            t_dimen[t_width] = list(set(t_dimen[t_width].add(t_height)))
                except Exception, e:
                    if t_width:
                        if t_height:
                            t_dimen[t_width] = list(set([t_height]))
        self.stdout.write(str(dimen))
        self.stdout.write(str(t_dimen))
        return {'dimen':dimen,'t_dimen':t_dimen}

    def get_number(self,line,attr):
        if(re.search(attr+'=\d+',line)):
           attr_value = re.search(attr+'=\d+',line).group()
           attr_value = re.split('=',attr_value)[1]
           return attr_value
        else:
            return None

    def intodb(self,filepath):
        from users.models import DevResolutions
        array = self.getall(filepath)
        for width,heights in array['dimen'].iteritems():
            for height in heights:
                new_reso = DevResolutions()
                new_reso.width = width
                new_reso.height = height
                new_reso.last_cropped_image = 0
                try:
                    new_reso.save()
                    print "Saved",width,"x",height
                    self.stdout.write("Saved",width,"x",height)
                except Exception:
                    pass
        for width,heights in array['t_dimen'].iteritems():
            for height in heights:
                new_reso = DevResolutions()
                new_reso.width = width
                new_reso.height = height
                try:
                    new_reso.save()
                    print "Saved",width,"x",height
                    self.stdout.write("Saved",width,"x",height)
                except Exception:
                    pass
