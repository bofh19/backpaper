__author__ = 'w4rlock'

import re
import operator

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    args = ''
    help = 'Get NAMES peeked (ie opened yetleast once) '

    def handle(self, *args, **options):
        self.stdout.write("Ranks Based on Peeked (ie opened to view list of images)")
        self.get_ranks2()

    def get_ranks(self):
        # f= open('/home/praveenr/backpaper_logs/access_backpaper.log')
        name_id_all = {}
        all_lines = []
        for line in f.readlines():
            all_lines.append(line)
        f.close()
        for i in range(1,8):
            f=open('/home/praveenr/backpaper_logs/access_backpaper.log.'+str(i))
            for line in f.readlines():
                all_lines.append(line)
            f.close()
        #import os
	#
        #for dirname, dirnames, filenames in os.walk('/home/praveenr/backpaper_logs/'):
        #    for filename in filenames:
        #        f = open(os.path.join(dirname, filename))
        #        for line in f.readlines():
        #            all_lines.append(line)
        #        f.close()
        from names.models import Name

        all_names = Name.objects.all()
        for line in all_lines:
            if (re.search('/api/v1/name_images/', line)):
                try:
                    name_id = re.search('name' + '=\d+', line).group()
                    name_id = re.split('=', name_id)[1]
                    try:
                        name_id_all[all_names.get(id=name_id).name] += 1
                    except Exception, e:
                        name_id_all[all_names.get(id=name_id).name] = 1
                except Exception, e:
                    pass
                    #self.stdout.write(str(e))
        name_id_sorted = sorted(name_id_all.iteritems(), key=operator.itemgetter(1))
        name_id_sorted.reverse()

        self.stdout.write(str(name_id_sorted))

    def get_ranks2(self):
        name_id_all = {}
        import os
        from names.models import Name

        all_names = Name.objects.all()
	loc = '/home/praveenr/logs/user/access_backpaper.log'
	loc2 = loc+'.'
	all_filenames = [loc,loc2+str(1),loc2+str(2),loc2+str(3),loc2+str(4),loc2+str(5),loc2+str(6),loc2+str(7)]
#        for dirname, dirnames, filenames in os.walk('/home/praveenr/backpaper_logs/'):
        for filename in all_filenames:
            self.stdout.write(filename)
            #f = open(os.path.join(dirname, filename))
	    f = open(filename)
            for line in f.readlines():
                if (re.search('/api/v1/name_images/', line)):
                    try:
                        name_id = re.search('name' + '=\d+', line).group()
                        name_id = re.split('=', name_id)[1]
                        try:
                            name_id_all[all_names.get(id=name_id).name] += 1
                        except Exception, e:
                            name_id_all[all_names.get(id=name_id).name] = 1
                    except Exception, e:
                        pass
                            #self.stdout.write(str(e))
            f.close()
        name_id_sorted = sorted(name_id_all.iteritems(), key=operator.itemgetter(1))
        name_id_sorted.reverse()
        for each_name in name_id_sorted:
            try:
                updating_name = all_names.get(name=each_name[0])
                updating_name.ranks_peeked = each_name[1]
                updating_name.save()
            except Exception, e:
                self.stdout.write(str(e))
        self.stdout.write(str(name_id_sorted))
