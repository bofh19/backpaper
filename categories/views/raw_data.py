from django.http import HttpResponse
mimetype='application/json'
data = """{"meta": {"limit": 20, "next": null, "offset": 0, "previous": null, "total_count": 6}, "objects": [{"categorie_adult": false, "categorie_enabled": true, "categorie_name": "Top Commented", "categorie_power": 15, "created_by": "M", "id": 6, "resource_uri": "/api/v1/categories/6/"}, {"categorie_adult": false, "categorie_enabled": true,"categorie_name": "Other", "categorie_power": 11, "created_by": "M", "id": 5, "resource_uri": "/api/v1/categories/5/"}, {"categorie_adult": false, "categorie_enabled": true, "categorie_name": "South Indian Actress", "categorie_power": 8, "created_by": "A", "id": 1, "resource_uri": "/api/v1/categories/1/"}, {"categorie_adult": false, "categorie_enabled": true,"categorie_name": "Bollywood", "categorie_power": 8, "created_by": "M", "id": 4, "resource_uri": "/api/v1/categories/4/"}]}"""
def raw_data(request,**kwargs):
	#print kwargs
	return HttpResponse(data,mimetype)
