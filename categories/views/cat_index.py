from django.shortcuts import render_to_response
from django.template import RequestContext	
from django.contrib.auth.decorators import login_required


from categories.models import *


def cat_index(request):
	categories_objects = Categories.objects.filter(categorie_enabled=True)[::-1]

	return render_to_response(
                                'categories/templates/cat_index.html',
                                {
                                 'categories_objects' : categories_objects,
                                },
                                context_instance=RequestContext(request))