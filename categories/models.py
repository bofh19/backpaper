from django.db import models

# Create your models here.

class Categories(models.Model):
	categorie_name = models.CharField(max_length=150,unique=True)
	categorie_enabled = models.BooleanField(default=True)
	categorie_power = models.IntegerField(default=0)
	created_by_choices = (
					(u'M',u'Manual'),
					(u'A',u'Auto')
							)

	created_by = models.CharField(max_length=1,choices=created_by_choices,default='M')
	categorie_adult = models.BooleanField(default=False)
	def __unicode__(self):
		return self.categorie_name

class Tags(models.Model):
	tag_name = models.CharField(max_length=150,unique=True)
	tag_enabled = models.BooleanField(default=True)
	def __unicode__(self):
		return self.tag_name
