__author__ = 'w4rlock'

from names.models import *
from django.contrib import admin
from django import forms
from categories.models import *
from images.models import *

from sorl.thumbnail import get_thumbnail

BANNER_THUMBS_SIZE_X = '480x800'
class name_info_admin(admin.ModelAdmin):
	model = Name
	list_display = ('image_thumb','name_to_images','name','categorie','name_power','name_enabled')
	list_filter = ['categorie']
	list_display_links = ('image_thumb','name')
	search_fields = ['name']
	readonly_fields = ['image_thumb']
	list_per_page = 500
	def image_thumb(self,obj):
		try:
			img = name_images.objects.filter(name=obj).order_by('-id')[0:1]
			crop_direction = 'center'
			crop_direction_val = img[0].crop_direction
			if crop_direction_val != 0:
				if(crop_direction_val == 1):
					crop_direction = 'right'
				elif(crop_direction_val == -1):
					crop_direction = 'left'
			thumb = get_thumbnail(img[0].imagefile, BANNER_THUMBS_SIZE_X,crop=crop_direction)
			return u'<img src="%s" />' % ( thumb.url)
		except Exception, e:
			return "No Image"
	def name_to_images(self,obj):
		if obj.id:
			url = "/admin/images/name_images/?name__id__exact=%s" % obj.id
			return "<a href='%s'>%s</a>" % (url,obj.name)
	name_to_images.short_description = 'Name To Images'
	name_to_images.allow_tags = True
	image_thumb.short_description = 'Image Thumbnail'
	image_thumb.allow_tags = True

class nameImageAdmin(admin.ModelAdmin):
	model = name_images
	list_display = ('id','image_thumb','name_with_url','name','ranks_used','image_enabled','image_adult')
	list_filter = ['name']
	list_display_links = ('image_thumb','name')
	readonly_fields = ['image_thumb']
	list_per_page = 500
	def image_thumb(self,obj):
		crop_direction = 'center'
		if obj.crop_direction != 0:
			if(obj.crop_direction == 1):
				crop_direction = 'right'
			elif(obj.crop_direction == -1):
				crop_direction = 'left'
		if obj.imagefile:
			try:
				thumb = get_thumbnail(obj.imagefile, BANNER_THUMBS_SIZE_X,crop=crop_direction)
				return "<img src='%s'/>" % (thumb.url)
			except Exception, e:
				return "cant crop"+str(e)
	def name_with_url(self,obj):
		if obj.name:
			url = "?name__id__exact=%s" % obj.name.id
			return "<a href='%s'>%s</a>" % (url,obj.name)
	name_with_url.short_description = 'Filter By Name'
	image_thumb.short_description = 'Image Thumbnail'
	name_with_url.allow_tags = True
	image_thumb.allow_tags = True

admin.site.register(Name,name_info_admin)
admin.site.register(Categories)
admin.site.register(Tags)
admin.site.register(name_images,nameImageAdmin)
