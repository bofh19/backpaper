from django.db import models

# Create your models here.
from categories.models import Categories


class Name(models.Model):
    name = models.CharField(max_length=150, unique=True)
    categorie = models.ForeignKey(Categories)
    name_enabled = models.BooleanField(default=True)
    name_power = models.IntegerField(default=0)
    created_by_choices = (
    (u'M', u'Manual'),
    (u'A', u'Auto')
    )

    created_by = models.CharField(max_length=1, choices=created_by_choices, default='M')
    ranks_used = models.IntegerField(default=0)
    ranks_peeked = models.IntegerField(default=0)
    avg_of_ranks = models.IntegerField(default=0, editable=False)
    comments_count = models.IntegerField(default=0,blank=True,null=True)
    total_votes_count = models.IntegerField(default=0,blank=True,null=True)
    sum_votes_count = models.IntegerField(default=0,blank=True,null=True)
    likes_votes_count = models.IntegerField(default=0,blank=True,null=True)
    dislikes_votes_count = models.IntegerField(default=0,blank=True,null=True)
    def save(self, *args, **kwargs):
        self.avg_of_ranks = (int)((self.ranks_used + self.ranks_peeked) / 2)
        super(Name, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name
