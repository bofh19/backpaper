def get_scaled_dimen(width,height):
    print width,height,
    try:
        width = int(width)
        height = int(height)
    except Exception, e:
        return(width,height)
    if(width>=1000):  # 1000 and above
        width = 1080
        height=1920
    elif (width>=600): # 600 - 1000 -> 720x1280
        width = 720
        height=1280
    elif (width>=500):  # 500-700 -> 540x976 loss - 60px
        width=540
        height=976
    elif (width >= 400):  # 400-500 480x850 loss - 20px
        width=480
        height=850
    elif (width>=300):   # 300-400 320x480 loss - 80px
        width=320
        height=480
    else:               # <300 240-300    loss - 60px
        width=240
        height=320
    print width,height
    return (width,height)

from django.shortcuts import render_to_response
from django.template import RequestContext  
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from django.core.cache import cache,get_cache
CONTENT_TYPE = 'application/json'
CACHE_TIMEOUT = 172800 #seconds 48 hours
def build_request(request):
    this_get = {}
    this_get = request.GET.copy()
    try:
        this_get['name']
    except Exception, e:
        this_get['order_by']='-id'
    width = -1
    height = -1
    device_height = -1
    device_width = -1
    try:
        if this_get['width'] and this_get['height']:
            width = this_get['width']
            height = this_get['height']
    except Exception, e:
        pass
        #print e
    #print width,height
    try:
        if this_get['device_width'] and this_get['device_height']:
            device_width = this_get['device_width']
            device_height = this_get['device_height']
    except Exception, e:
        pass
        #print e
    if width is not -1 and height is not -1:
     #   print width,height
        width,height = get_scaled_dimen(width,height)
        this_get['width'] = width
        this_get['height'] = height
    if device_height is not -1 and device_width is not -1:
      #  print device_width,device_height
        device_width,device_height = get_scaled_dimen(device_width,device_height)
        this_get['device_width'] = device_width
        this_get['device_height'] = device_height
    request.GET = this_get
    return request

from names.api import name_images_resource
def name_images_cached(request):
    request = build_request(request)
    unique_key = 'nic_'
    try:
        unique_key += request.GET['format']
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['order_by'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['limit'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['offset'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['name'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['device_width'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['device_height'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['width'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['height'])
    except Exception, e:
        pass
    print unique_key,
    #print request.GET
    # print resp.content
    file_cache = get_cache('file_cache')
    try:
        result_json = file_cache.get(unique_key)
        if result_json:
            print 'cache_hit'
            return HttpResponse(result_json,CONTENT_TYPE)
    except Exception, e:
        print e
    nir = name_images_resource()
    resp=nir.get_list(request)
    print 'cache_set'
    file_cache.set(unique_key,resp.content,CACHE_TIMEOUT)
    return resp

###-------------------------------
###------------------------------


from categories.models import Categories
try:
    top_comment_cat_id = Categories.objects.get(categorie_name="Top Commented").id
    #print top_comment_cat_id
except Exception, e:
    top_comment_cat_id = None
def build_request_name_cached(request):
    if top_comment_cat_id != None:
        this_get = {}
        this_get = request.GET.copy()
        try:
            cat = this_get['categorie__id']
            if(int(cat) == int(top_comment_cat_id)):
                del this_get['categorie__id']
                this_get['order_by'] = '-comments_count'
            this_get['limit']=0
        except Exception, e:
            pass
        request.GET = this_get
    return request

from names.api import NamesResource
from images.views.multi_upload import get_proper_filename
def name_cached(request):
    request = build_request_name_cached(request)
    unique_key = 'nr'
    try:
        unique_key += str(request.GET['format'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['order_by'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['categorie__id'])
    except Exception, e:
        pass
    try:
        unique_key+=str(request.GET['limit'])
    except Exception, e:
        pass
    try:
        unique_key+=get_proper_filename(str(request.GET['name__icontains']))
    except Exception, e:
        pass
    print unique_key,
    file_cache = get_cache('file_cache')
    try:
        result_json = file_cache.get(unique_key)
        if result_json:
            print 'cache_hit'
            return HttpResponse(result_json,CONTENT_TYPE)
    except Exception, e:
        print e
    nr = NamesResource()
    resp=nr.get_list(request)
    print 'cache_set'
    file_cache.set(unique_key,resp.content,CACHE_TIMEOUT)
    return resp
