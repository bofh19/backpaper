from django.contrib.auth.models import User
from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS, Resource

from names.models import Name
from categories.models import Categories
from images.models import name_images
from users.models import DeviceAccessors
from users.models import DeviceNotifications

from scomments.models import sComments
from scomments.models import sLikes
from tastypie.cache import SimpleCache

try:
    top_comment_cat_id = Categories.objects.get(categorie_name="Top Commented").id
    #print top_comment_cat_id
except Exception, e:
    top_comment_cat_id = None

from django.conf.urls import url
#top_comment_cat_id = 3
class CategoriesResource(ModelResource):
    class Meta:
        queryset = Categories.objects.filter(categorie_enabled=True).order_by('-categorie_power', 'id')
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        resource_name = 'categories.json'
        resource_name = 'categories'
        filtering = {
        'categorie_name': ALL,
        'categorie_adult': ['exact'],
        'id': ['exact'],
        }
        cache = SimpleCache(timeout=10)
    # def override_urls(self):
    #     return [
    #         url(r"^(?P<resource_name>%s)/$" % self._meta.resource_name,'categories.views.raw_data', name="categories_views_raw_data"),
    #     ]
    def determine_format(self, request):
        return 'application/json'

class sCommentsResource(ModelResource):
    class Meta:
        queryset = sComments.objects.filter(comment_disabled=False,comment_deleted=False).order_by('-modified_date')
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        resource_name = 'comments'
        filtering = {
        'unique_group_id': ALL,
        'given_tstamp':ALL,
        }
        excludes = ['email','extra_field']
        limit = 0
        cache = SimpleCache(timeout=10)

class sLikesResource(ModelResource):
    class Meta:
        queryset = sLikes.objects.filter().order_by('-modified_date')
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        resource_name = 'likes'
        filtering = {
            'unique_group_id' : ALL,
            'given_tstamp':ALL,
            'dev_id':ALL
        }
        limit = 0
        cache = SimpleCache(timeout=10)

class NamesResource(ModelResource):
    categorie = fields.ForeignKey(CategoriesResource, 'categorie', full=True)

    name_images = fields.ToManyField('names.api.name_images_resource',
                                     attribute=lambda bundle: name_images.objects.filter(name=bundle.obj).order_by(
                                         '-id')[0:1], full=True, null=True)

    class Meta:
        queryset = Name.objects.filter(name_enabled=True, categorie__categorie_enabled=True).order_by('-name_power',
                                                                                                      'name', 'id')
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        resource_name = 'name'
        filtering = {
        'name': ALL,
        'categorie': ALL_WITH_RELATIONS,

        }
        ordering = ['id','comments_count']
        cache = SimpleCache(timeout=10)

    def build_bundle(self, obj=None, data=None, request=None, objects_saved=None):
        if obj is None and self._meta.object_class:
            obj = self._meta.object_class()

        if top_comment_cat_id != None:
            this_get = {}
            this_get = request.GET.copy()
            try:
                cat = this_get['categorie__id']
                if(int(cat) == int(top_comment_cat_id)):
                    del this_get['categorie__id']
                    this_get['order_by'] = '-comments_count'
		    this_get['limit']=0
            except Exception, e:
                pass
            request.GET = this_get
        #print request.GET
        return Bundle(
            obj=obj,
            data=data,
            request=request,
            objects_saved=objects_saved
        )

    def dehydrate(self, bundle):
        return bundle


class name_images_resource(ModelResource):
    name = fields.ForeignKey(NamesResource, 'name')

    class Meta:
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        authorization = Authorization()
        queryset = name_images.objects.filter(image_enabled=True, name__name_enabled=True,
                                              name__categorie__categorie_enabled=True)
        resource_name = 'name_images'
        filtering = {
        'name': ['exact'],
        'id': ['exact', 'gt', 'lt', 'gte', 'lte', 'range'],
        'image_adult': ['exact'],
        }
        ordering = ['id']
        cache = SimpleCache(timeout=10)
    def build_bundle(self, obj=None, data=None, request=None, objects_saved=None):
        if obj is None and self._meta.object_class:
            obj = self._meta.object_class()

        this_get = {}
        this_get = request.GET.copy()
        try:
            this_get['name']
        except Exception, e:
            this_get['order_by']='-id'
        width = -1
        height = -1
        device_height = -1
        device_width = -1
        try:
            if this_get['width'] and this_get['height']:
                width = this_get['width']
                height = this_get['height']
        except Exception, e:
            pass
            #print e
        #print width,height
        try:
            if this_get['device_width'] and this_get['device_height']:
                device_width = this_get['device_width']
                device_height = this_get['device_height']
        except Exception, e:
            pass
            #print e
        if width is not -1 and height is not -1:
         #   print width,height
            width,height = get_scaled_dimen(width,height)
            this_get['width'] = width
            this_get['height'] = height
        if device_height is not -1 and device_width is not -1:
          #  print device_width,device_height
            device_width,device_height = get_scaled_dimen(device_width,device_height)
            this_get['device_width'] = device_width
            this_get['device_height'] = device_height
        request.GET = this_get
        return Bundle(
            obj=obj,
            data=data,
            request=request,
            objects_saved=objects_saved
        )

    def dehydrate(self, bundle):
        try:
            bundle.data['name_name'] = bundle.obj.name
        except Exception, e:
            print 'error while getting name '
            print e
            bundle.data['name_name'] = 'No Name'
        BANNER_THUMBS_SIZE = '100x160'
        from sorl.thumbnail import get_thumbnail

        bundle.data["imagefile_download"] = bundle.data["imagefile"]
        try:
            if bundle.request.GET['width'] and bundle.request.GET['height']:
                BANNER_THUMBS_SIZE = str(bundle.request.GET['width']) + 'x' + str(bundle.request.GET['height'])
        except Exception, e:
            pass
        try:
            if(bundle.obj.crop_direction == name_images.center):
                crop_direction = 'center'
            elif bundle.obj.crop_direction == name_images.right:
                crop_direction = 'right'
            elif bundle.obj.crop_direction == name_images.left:
                crop_direction = 'left'
        except Exception, e:
            crop_direction = 'center'
        try:
            if bundle.request.GET['device_width'] and bundle.request.GET['device_height']:
                BANNER_THUMBS_SIZE_X = str(bundle.request.GET['device_width']) + 'x' + str(
                    bundle.request.GET['device_height'])
                bundle.data["imagefile"] = get_thumbnail(bundle.data["imagefile"][7:], BANNER_THUMBS_SIZE_X,
                                                         crop=crop_direction).url
        except Exception, e:
            pass
        try:
            bundle.data["imagefile_small"] = get_thumbnail(bundle.data["imagefile"][7:], BANNER_THUMBS_SIZE,
                                                           crop=crop_direction).url
        except Exception, e:
            bundle.data["imagefile_small"] = get_thumbnail('site_media/common/no_image.jpg', BANNER_THUMBS_SIZE,
                                                           crop='center').url
        return bundle

    # def build_filters(self, filters=None):
    #   if filters is None:
    #       filters = {}
    #   if('name' in filters):
    #       print int(filters['name'])
    #       if(int(filters['name'])==-1):
    #           del filters['name']
    #   orm_filters = super(name_images_resource, self).build_filters(filters)
    #   return orm_filters
def get_scaled_dimen(width,height):
    try:
        width = int(width)
        height = int(height)
    except Exception, e:
        return(width,height)
    if(width>=1000):  # 1000 and above
        width = 1080
        height=1920
    elif (width>=600): # 600 - 1000 -> 720x1280
        width = 720
        height=1280
    elif (width>=500):  # 500-700 -> 540x976 loss - 60px
        width=540
        height=976
    elif (width >= 400):  # 400-500 480x850 loss - 20px
        width=480
        height=850
    elif (width>=300):   # 300-400 320x480 loss - 80px
        width=320
        height=480
    else:               # <300 240-300    loss - 60px
        width=240
        height=320
    return (width,height)

class NamesPopularResource(NamesResource):
    class Meta:
        resource_name = 'names_popular'
        queryset = Name.objects.filter(name_enabled=True, categorie__categorie_enabled=True).order_by('-avg_of_ranks',
                                                                                                      'name', 'id')
    def dehydrate(self, bundle):
        bundle.data["name"] = bundle.data["name"]+" ("+str(bundle.data["avg_of_ranks"])+")"
        return bundle

class name_images_resource_popular(ModelResource):
    name = fields.ForeignKey(NamesResource, 'name')

    class Meta:
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        authorization = Authorization()
        queryset = name_images.objects.filter(image_enabled=True, name__name_enabled=True,
                                              name__categorie__categorie_enabled=True).order_by('-ranks_used')
        resource_name = 'name_images_popular'
        filtering = {
        'name': ['exact'],
        'id': ['exact', 'gt', 'lt', 'gte', 'lte', 'range'],
        'image_adult': ['exact'],
        }
        ordering = ['id']
        cache = SimpleCache(timeout=10)

    def dehydrate(self, bundle):
        try:
            bundle.data['name_name'] = bundle.obj.name
        except Exception, e:
            print 'error while getting name '
            print e
            bundle.data['name_name'] = 'No Name'
        BANNER_THUMBS_SIZE = '100x160'
        from sorl.thumbnail import get_thumbnail

        bundle.data["imagefile_download"] = bundle.data["imagefile"]
        try:
            if bundle.request.GET['width'] and bundle.request.GET['height']:
                BANNER_THUMBS_SIZE = str(bundle.request.GET['width']) + 'x' + str(bundle.request.GET['height'])
        except Exception, e:
            pass
        try:
            if(bundle.obj.crop_direction == name_images.center):
                crop_direction = 'center'
            elif bundle.obj.crop_direction == name_images.right:
                crop_direction = 'right'
            elif bundle.obj.crop_direction == name_images.left:
                crop_direction = 'left'
        except Exception, e:
            crop_direction = 'center'
        try:
            if bundle.request.GET['device_width'] and bundle.request.GET['device_height']:
                BANNER_THUMBS_SIZE_X = str(bundle.request.GET['device_width']) + 'x' + str(
                    bundle.request.GET['device_height'])
                bundle.data["imagefile"] = get_thumbnail(bundle.data["imagefile"][7:], BANNER_THUMBS_SIZE_X,
                                                         crop=crop_direction).url
        except Exception, e:
            pass
        try:
            bundle.data["imagefile_small"] = get_thumbnail(bundle.data["imagefile"][7:], BANNER_THUMBS_SIZE,
                                                           crop=crop_direction).url
        except Exception, e:
            bundle.data["imagefile_small"] = get_thumbnail('site_media/common/no_image.jpg', BANNER_THUMBS_SIZE,
                                                           crop='center').url
        return bundle

    

class DeviceNotificationsResource(ModelResource):
    class Meta:
        authorization = Authorization()
        queryset = DeviceNotifications.objects.all()
        list_allowed_methods = ['post', 'put']
        detail_allowed_methods = ['post', 'put']
        resource_name = 'deviceNotify'
        filtering = {
        'dev_id': ['exact'],
        'id': ['exact'],
        }
    # def obj_create(self,bundle,**kwargs):
    #   querydict = bundle.request.POST
    #   print querydict
    #   mydict = querydict.dict()
    #   print mydict
    #   for key, value in kwargs.items():
    #       print "key ",key
    #       print "value ",value
    #   print bundle.obj
    #   try:
    #       super(DeviceNotificationsResource,self).obj_create(bundle,**kwargs)
    #   except Exception, e:
    #       print e


# curl --dump-header - -H "Content-Type: application/json" -X POST --data '{"dev_id": "from curl 1", "is_active": false, "reg_id": "from curl regid 1"}' http://127.0.0.1:8000/api/v1/deviceNotify/


# curl --dump-header - -H "Content-Type: application/json" -X PUT --data '{"dev_id": "fromcurl1", "is_active": false, "reg_id": "from curl regid 1"}' http://127.0.0.1:8000/api/v1/deviceNotify/fromcurl1/
from tastypie.bundle import Bundle


class InfoObject(object):
    def __init__(self, initial=None):
        self.__dict__['_data'] = {}
        self.__dict__['_data']['uuid'] = 1
        self.__dict__['_data']['categories_count'] = Categories.objects.filter(categorie_enabled=True).count()
        self.__dict__['_data']['names_count'] = Name.objects.filter(name_enabled=True).count()
        self.__dict__['_data']['images_count'] = name_images.objects.filter(image_enabled=True).count()
        self.__dict__['_data']['ServerVersion'] = "1.22"
        self.__dict__['_data']['addEnabled'] = "true"
        self.__dict__['_data']['cache_version'] = '0.2'

    def __getattr__(self, name):
        pass

    def __setattr__(self, name, value):
        pass

    def to_dict(self):
        return self._data


class AboutInfoResource(Resource):
    class Meta:
        resource_name = 'info'
        object_class = InfoObject

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}
        bundle_or_obj.data = bundle_or_obj.obj
        return kwargs

    def get_objects_list(self, request):
        results = []
        obj = InfoObject()
        results.append(obj.to_dict())
        return results

    def obj_get(self, request=None, **kwargs):
        obj = InfoObject()
        return obj

    def obj_get_list(self, request=None, **kwargs):
        return self.get_objects_list(request)

    def obj_create(self, bundle, request=None, **kwargs):
        pass

    def obj_update(self, bundle, request=None, **kwargs):
        pass

    def obj_delete_list(self, request=None, **kwargs):
        pass

    def obj_delete(self, request=None, **kwargs):
        pass

    def rollback(self, bundles):
        pass
