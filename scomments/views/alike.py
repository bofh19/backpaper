__author__ = 'w4rlock'

from django.http import HttpResponse
from django.middleware.csrf import get_token
# import random
# import json
# import re
#
# from datetime import *
# from django.views.decorators.csrf import csrf_protect
# from django.views.decorators.csrf import requires_csrf_token
# from django.views.decorators.csrf import ensure_csrf_cookie

from apost import DEV_KEYS

from hmac import new as hmac
from scomments.models import sLikes

def alike(request):
    if request.POST:
        post = request.POST
        #print post
        try:
            rank_given = post['rank_given']
        except Exception, e:
            return HttpResponse("failed rank_given data")
        try:
            tstamp = post['tstamp']
        except Exception, e:
            return HttpResponse("failed tstamp")
        try:
            rank_uid = post['rank_uid']
        except Exception, e:
            return HttpResponse("failed rank uid")
        try:
            rank_given = int(rank_given)
        except Exception, e:
            return HttpResponse("Unable to Convert to int")
        try:
            current_key = DEV_KEYS[int(tstamp[len(tstamp) - 1])]
        except Exception, e:
            return HttpResponse("failed !!!!")
        try:
            hmac_post = post['secret_key']
        except Exception, e:
            return HttpResponse("failed wtf r u trying to do")
            # pass #fow now
        try:
            like_dev_id = post['dev_id']
        except Exception, e:
            like_dev_id = ''
        hmac_message = tstamp + str(rank_given) + rank_uid
        hmac_key = current_key
        # hmac_server = hmac(hmac_key, hmac_message, sha1).digest().encode('base64')[:-1]
        hmac_server = hmac(hmac_key, hmac_message).hexdigest()
        #  JAVA Equals below one
        #    	public static class HashingUtility {
        #     public static String HMAC_MD5_encode(String key, String message) throws Exception {
        #         SecretKeySpec keySpec = new SecretKeySpec(
        #                 key.getBytes(),
        #                 "HmacMD5");
        #         Mac mac = Mac.getInstance("HmacMD5");
        #         mac.init(keySpec);
        #         byte[] rawHmac = mac.doFinal(message.getBytes());
        #         return Hex.encodeHexString(rawHmac);
        #     }
        # }
        #print hmac_server
        if hmac_server == hmac_post:#True:  #
            try:
                old_rank = sLikes.objects.get(unique_group_id=rank_uid,dev_id=like_dev_id)
                old_rank.rank_given = rank_given
                try:
                    old_rank.save()
                    return HttpResponse("Sucess - Updated")
                except Exception, e:
                    return HttpResponse("failed")
            except Exception,e :
                pass
            new_rank = sLikes()
            new_rank.unique_group_id = rank_uid
            new_rank.rank_given = rank_given
            new_rank.dev_id = like_dev_id
            new_rank.given_tstamp = tstamp
            try:
                new_rank.save()
                return HttpResponse("Sucess")
            except Exception, e:
                return HttpResponse("failed")
        else:
            print "----------- HMAC ERROR ---------------"
            print request
            return HttpResponse("failed")
    # https://docs.djangoproject.com/en/dev/ref/contrib/csrf/
    return HttpResponse(get_token(request))
    # return render_to_response('scomments/templates/index.html',
    #     {
    #     }, context_instance=RequestContext(request)
    # )
#=sLikes.objects.filter(unique_group_id='test').annotate(ranks_sum=Sum('rank_given'))
from django.db.models import Sum as AnnotateSum
import json
mimetype='application/json'
def get_likes_sum(request):
    if(request.GET.get('unique_group_id')):
        unique_group_id = request.GET.get('unique_group_id')
        objects = sLikes.objects.filter(unique_group_id=unique_group_id).aggregate(ranks_sum=AnnotateSum('rank_given'))
        if(objects['ranks_sum'] is None):
            objects['ranks_sum'] = 0
        objects['unique_group_id'] = unique_group_id
        objects['total_no_ranks'] = sLikes.objects.filter(unique_group_id=unique_group_id).count()
        objects_final={}
        objects_final['meta'] = objects
        result_json = json.dumps(objects_final)
        return HttpResponse(result_json,mimetype)
    else:
        objects_final={}
        objects_final['meta'] = {}
        result_json = json.dumps(objects_final)
        return HttpResponse(result_json,mimetype)