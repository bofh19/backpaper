__author__ = 'w4rlock'

from scomments.models import *
from django.contrib import admin

class slikes_info_admin(admin.ModelAdmin):
	model = sLikes
	list_display=('id','unique_group_id','link_to_object','rank_given_int','rank_given')
	def rank_given_int(self,obj):
		try:
			return str(obj.rank_given)
		except Exception, e:
			return "something failed"	

	def link_to_object(self,obj):
		if(obj.unique_group_id):
			z=obj.unique_group_id.split('_')
			if(len(z)==2):
				if(z[0]=='A'):
					url = "/admin/names/name/%s/"%z[1]
					return "<a href='%s'>%s</a>"%(url,"link_to_name")
				elif (z[0]=='I'):
					url = "/admin/images/name_images/%s/"%z[1]
					return "<a href='%s'>%s</a>"%(url,"link_to_image")
				else:
					return "None"
		else:
			return "None"
	link_to_object.short_description = 'link to object'
	link_to_object.allow_tags = True

class scomments_info_admin(admin.ModelAdmin):
	model = sComments
	list_display = ('id','unique_group_id','link_to_object','comment_data','name')
	
	def link_to_object(self,obj):
		if(obj.unique_group_id):
			z=obj.unique_group_id.split('_')
			if(len(z)==2):
				if(z[0]=='A'):
					url = "/admin/names/name/%s/"%z[1]
					return "<a href='%s'>%s</a>"%(url,"link_to_name")
				elif (z[0]=='I'):
					url = "/admin/images/name_images/%s/"%z[1]
					return "<a href='%s'>%s</a>"%(url,"link_to_image")
				else:
					return "None"
		else:
			return "None"	
	link_to_object.short_description = 'link to object'
	link_to_object.allow_tags = True

admin.site.register(sComments,scomments_info_admin)
admin.site.register(sLikes,slikes_info_admin)