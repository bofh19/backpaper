__author__ = 'w4rlock'

from django.conf.urls import patterns, url

urlpatterns = patterns('scomments.views',

                       url(r'^apost/$', 'apost', name='apost'),
                       url(r'^alike/$', 'alike', name='alike'),
                        url(r'^get_likes_sum/$', 'get_likes_sum', name='get_likes_sum'),

)
