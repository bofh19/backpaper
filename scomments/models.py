from django.db import models

# Create your models here.

#reg = re.compile('^[a-zA-Z0-9_]+$')

import re
from django.core.exceptions import ValidationError


class sComments(models.Model):
    unique_group_id = models.CharField(max_length=255, verbose_name="UniqueGroupID")
    comment_data = models.TextField(verbose_name="Comment Data")
    name = models.CharField(max_length=45, verbose_name="Name", blank=True, null=True, default="Anonymous")
    email = models.CharField(max_length=45, verbose_name="Email", blank=True, null=True,
                             default="anonymous@anonymous.com")
    extra_field = models.CharField(max_length=45, verbose_name="Extra Field", null=True, blank=True)

    given_tstamp = models.CharField(max_length=30, blank=True, null=True)
    creation_date = models.DateTimeField(verbose_name="Creation date", auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name="Modified date", auto_now=True)

    comment_disabled = models.BooleanField(default=False)
    comment_deleted = models.BooleanField(default=False)

    def __unicode__(self):
        return self.unique_group_id + " - " + self.comment_data

    class Meta:
        ordering = ['-modified_date']

    def save(self):
        reg = re.compile('^[a-zA-Z0-9_]+$')
        if reg.match(self.unique_group_id):
            super(sComments, self).save()
        else:
            raise ValidationError("Invalid Group Id Given To Save")


class sLikes(models.Model):
    unique_group_id = models.CharField(max_length=255, verbose_name="UniqueGroupID")
    one = 1
    zero = 0
    minus_one = -1
    RANK_CHOICES = (
    (one, u'one'),
    (zero, u'zero'),
    (minus_one, u'minus_one'),
    )
    rank_given = models.IntegerField(verbose_name=("Rank Given"), choices=RANK_CHOICES,default=zero)

    given_tstamp = models.CharField(max_length=30)
    creation_date = models.DateTimeField(verbose_name="Creation date", auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name="Modified date", auto_now=True)

    dev_id = models.CharField(max_length=45, verbose_name="Dev Id")

    def __unicode__(self):
        return self.unique_group_id + " / " + str(self.rank_given)

    class Meta:
        ordering = ['-modified_date']
        unique_together = ("unique_group_id", "dev_id")

    def save(self):
        reg = re.compile('^[a-zA-Z0-9_]+$')
        if reg.match(self.unique_group_id):
            super(sLikes, self).save()
        else:
            raise ValidationError("Invalid Group Id Given To Save")