var NameImagesModel = Backbone.Model.extend({

	urlRoot: '/api/v1/name_images/',
	defaults: {
		context_source: "context_source",
		creation_date: "",
		crop_direction: 0,
		date: "",
		id: -1,
		image_adult: false,
		image_enabled: true,
		imagefile: "image_file",
		imagefile_download: "actual_image_path",
		imagefile_small: "image_file_thumbnail",
		name: "image_name_uri",
		name_name: "image_name",
		ranks_used: 0,
		resource_uri: "resource uri",
		source: "image_downloaded source"
	}
})