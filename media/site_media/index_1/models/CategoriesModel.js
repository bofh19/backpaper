var CategoriesModel = Backbone.Model.extend({

	urlRoot: '/api/v1/categories/',
	defaults: {
		categorie_adult: false,
		categorie_enabled: true,
		categorie_name: 'default categorie name',
		categorie_power: 0,
		created_by: 'M',
		id: -1,
		resource_uri: "/api/v1/categories/-1/"
	}
})