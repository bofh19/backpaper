var NamesModel = Backbone.Model.extend({

	urlRoot: '/api/v1/name/',
	defaults: {
		avg_of_ranks: 0,
		categorie: {
			categorie_adult: false,
			categorie_enabled: true,
			categorie_name: "NONE",
			categorie_power: 0,
			created_by: "A",
			id: -1,
			resource_uri: "resource_uri"
		},
		comments_count: 0,
		created_by: "A",
		id: -1,
		name: "NONE",
		name_enabled: true,
		name_images: [
		{
			context_source: "NONE",
			creation_date: "NONE",
			crop_direction: 0,
			date: "NONE",
			id: -1,
			image_adult: false,
			image_enabled: true,
			imagefile: "NONE",
			imagefile_download: "NONE",
			imagefile_small: "NONE",
			name: "NONE",
			name_name: "NONE",
			ranks_used: 0,
			resource_uri: "NONE",
			source: "NONE"
		}
		],
		name_power: 0,
		ranks_peeked: 0,
		ranks_used: 0,
		resource_uri: "NONE"
	}
})