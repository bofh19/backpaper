

var LoadingItemView = Backbone.View.extend({


	template: Handlebars.compile(
		'<div class="progress progress-striped active"><div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>'
	),
	
	render: function  () {
		this.$el.html(this.template(this.collection));
		return this;
	}
});