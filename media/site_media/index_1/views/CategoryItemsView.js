var CategoryItemsView = Backbone.View.extend({

	// initialize: function (attrs) {
 //    	this.options = attrs;
	// },

	initialize: function(){
		
		this.listenTo(this.collection, "reset", this.render);
		
	},

	template: Handlebars.compile(
		'<ul class="nav nav-pills" >' + 
		'{{#each models}}<li> <a href="#/category/{{attributes.id}}">{{attributes.categorie_name}}</a></li>{{/each}}' + 
		'</ul>'
	),
	
	render: function  () {
		this.$el.html(this.template(this.collection));
		return this;
	}
});