var NameItemsView = Backbone.View.extend({

	// initialize: function (attrs) {
 //    	this.options = attrs;
	// },

	initialize: function(){
		
		this.listenTo(this.collection, "reset", this.render);
		
	},

	template: Handlebars.compile(
	'<div class="row">' +
		'{{#each models}}' +
	        '<div class="col-sm-6 col-md-3 image_thumb">' +
	          '<a href="#/name/{{attributes.id}}" class="thumbnail remove_spaces">' +
	            '<img src="{{attributes.name_images.0.imagefile}}" style="display: block;">' +
	          '</a>' +
	          '<h5 class="text_on_image">{{attributes.name}}</h5>'+
	        '</div>' +
	    '{{/each}}' +
	'</div>'
	),
	
	render: function  () {
		this.$el.html(this.template(this.collection));
		return this;
	}
});