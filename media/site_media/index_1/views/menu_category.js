var MenuCategoryView = Backbone.View.extend({

	// initialize: function (attrs) {
 //    	this.options = attrs;
	// },

	initialize: function(){
		this.listenTo(this.model,"change",this.render);
	},

	template: Handlebars.compile(
		'<h1>{{categorie_name}}</h1>' 
	),
	
	render: function  () {
		console.log(this.model.attributes)
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});