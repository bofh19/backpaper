var AppRouter = Backbone.Router.extend({
	routes: {
		"": "list",
		"category/:category_id_given": "categoryDetails",
		"name/:name_id_given": "nameDetails",
	},

	initialize: function  () {
		this.categoryItems = new CategoriesCollections([],{});
		//this.categoryItems.fetch({reset: true});
		this.loadingItemView = new LoadingItemView();
		this.categoryItemsView = new CategoryItemsView(
			{
				collection: this.categoryItems
			}
		);
		$("#main_nav_items").html(this.categoryItemsView.render().el);
	},

	list: function () {
			$("#main_nav_items").html(this.categoryItemsView.render().el);
	},
	nameDetails: function  (name_id_given) {
		this.list();
		this.nameImagesItems = new NameImagesCollections([],{device_height:800,device_width:480,name_id:name_id_given});
		this.nameImagesItems.fetch({reset:true});
		this.nameImagesView = new NameImagesView({
			collection: this.nameImagesItems
		});
		$('#app').html(this.nameImagesView.render().el);
	},

	categoryDetails: function(category_id_given){
		this.list();
		//this.nameItems = new NamesCollections([],{device_height:800,device_width:480,category_id_given:category_id_given})
		this.nameItems = new NamesCollections()
		// this.nameItems.fetch({reset:true});
		this.nameItems.reset(names[category_id_given])
		this.nameItemsView = new NameItemsView({
			collection: this.nameItems
		});
		$('#app').html(this.nameItemsView.render().el);
	}
});

var app = new AppRouter();

$(function() {
	Backbone.history.start();
});

$(document).ajaxStart(function(){
  $("#loading_element").show();
});

$(document).ajaxStop(function(){
  $("#loading_element").hide();
});
