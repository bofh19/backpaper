from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse


import json
import urllib2
import urllib

def actors_images(request):
    if request.user.is_staff:
        pass
    else:
        return HttpResponseRedirect(str(reverse('django.contrib.auth.views.login')) + '?next=' + request.path)
    outputx = []

    try:
        query_string = request.GET['q']
    except Exception, e:
        query_string = None
    try:
        g_start = request.GET['g_start']
    except Exception, e:
        g_start = 0
    g_start_next_url = "/"
    if query_string != None:
        query = urllib.urlencode({'q': query_string, 'start': g_start})
        urlx = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&rsz=8&imgsz=xxlarge"
        url =  urlx+"&%s" % query
        opener = urllib2.build_opener()
        opener.addheader = [
            ('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]
        f = urllib2.urlopen(url)
        gdata = json.loads(f.read())

        for each_img in gdata['responseData']['results']:
            this_output_dict = {}
            this_output_dict['originalContextUrl'] = each_img['originalContextUrl']
            this_output_dict['url'] = each_img['url']
            this_output_dict['unescapedUrl'] = each_img['unescapedUrl']
            this_output_dict['tbUrl'] = each_img['tbUrl']
            this_output_dict['width'] = each_img['width']
            this_output_dict['height'] = each_img['height']
            this_output_dict['imageId'] = each_img['imageId']
            outputx.append(this_output_dict)
        pages_array = gdata['responseData']['cursor']['pages']
        g_start_next = 0
        g_start = str(g_start)
        for counter,each_page in enumerate(pages_array):
            if g_start == each_page['start']:
                g_start_next = pages_array[counter+1]['start']
                break
        g_start_next_url = reverse('gparser.views.actors_images',)
        g_start_next_url = g_start_next_url + "?"+urllib.urlencode({'q':query_string,'g_start':g_start_next})
        print g_start_next_url
    return render_to_response('gparser/templates/add_actor_images.html',
                              {
                                  'output': outputx,
                                  'g_start_next':g_start_next_url
                              }, context_instance=RequestContext(request)
    )


from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import ensure_csrf_cookie

from images.views.multi_upload import get_name_instance
from images.views.multi_upload import handle_upload_url_file
from images.views.multi_upload import get_proper_filename

from django.core.files import File

from images.models import name_images


@ensure_csrf_cookie
@requires_csrf_token
@csrf_protect
def actors_images_ajax_post(request):
    print request.POST
    if request.user.is_staff:
        pass
    else:
        return HttpResponse("WHY DID IT FAILED ?")

    try:
        post = request.POST
    except Exception, e:
        return HttpResponse("WHY DID IT FAILED 1 ?")
    try:
        name_instance = get_name_instance(request)
    except Exception, e:
        return HttpResponse("WHY DID IT FAILED 2 ?")

    try:
        url = request.POST['url']
    except Exception, e:
        print e
        return HttpResponse("WHY DID IT FAILED 3 ?")

    try:
        context_url = request.POST['originalContextUrl']
    except Exception, e:
        context_url = ""

    message = ""
    try:
        new_image = name_images()
        new_image.name = name_instance
        new_image.source = url
        new_image.context_source = context_url
        filename = url.split('/')[-1]
        slug_filename = get_proper_filename(filename)
        new_image.image_adult = name_instance.categorie.categorie_adult
        new_image.uploaded_by = request.user
        new_image.imagefile.save(slug_filename, File(handle_upload_url_file(url)))
        message = message + "saved on url " + url + "<br/>"
    except Exception, e:
        message = message + str(e)

    return HttpResponse(message)