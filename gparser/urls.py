from django.conf.urls import patterns, include, url

urlpatterns = patterns('gparser.views',
                       url(r'^actors_images/$', 'actors_images', name='actors_images'),
                       url(r'^actors_images_ajax_post/$', 'actors_images_ajax_post', name='actors_images_ajax_post'),
)
