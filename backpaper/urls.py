from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf.urls.defaults import *
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from names.api import name_images_resource,NamesResource
from names.api import CategoriesResource,DeviceNotificationsResource
from names.api import AboutInfoResource
from names.api import name_images_resource_popular,NamesPopularResource
from names.api import sCommentsResource,sLikesResource
from tastypie.api import Api

v1_api = Api(api_name='v1')
v1_api.register(name_images_resource())
v1_api.register(NamesResource())
v1_api.register(CategoriesResource())
v1_api.register(DeviceNotificationsResource())
v1_api.register(AboutInfoResource())
v1_api.register(name_images_resource_popular())
v1_api.register(NamesPopularResource())
v1_api.register(sCommentsResource())
v1_api.register(sLikesResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'backpaper.views.home', name='home'),
    # url(r'^backpaper/', include('backpaper.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^img/',include('images.urls')),
      url(r'^api/v1/categories/','categories.views.raw_data'),
    url(r'^api/v1/',include('names.urls')),
     url(r'^api/', include(v1_api.urls)),
     # url(r'', include('gcm.urls')),

     url(r'^user/',include('users.urls')),

     url(r'^cat/',include('categories.urls')),
     url(r'^indexv1/',include('indexv1.urls')),
     url(r'^feedback/',include('feedback.urls')),
     url(r'^gparser/',include('gparser.urls')),
     url(r'^scomments/',include('scomments.urls')),

     url(r'^accounts/login/$', 'django.contrib.auth.views.login',name="user_login"),
     url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',{'template_name': 'registration/logout.html'},name='user_logout'),

     url(r'', include('social_auth.urls')),

)+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

urlpatterns += patterns('indexv1.views',
    url(r'^policy/$','policy',name="policy"),
    url(r'^$','main',name="main"),
    )

handler404 = 'indexv1.views.my_404_view'
