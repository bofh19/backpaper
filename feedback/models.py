from django.db import models

# Create your models here.
from images.models import name_images


class ImageReporting(models.Model):
    image = models.ForeignKey(name_images, verbose_name=("Image"))
    name = models.CharField(max_length=45, verbose_name=("Name"))
    email = models.CharField(max_length=45, verbose_name=("Email"))
    short_reason = models.CharField(max_length=255, verbose_name=("Reason Short"), null=True, blank=True)
    long_reason = models.TextField(verbose_name=("Reason Login"), null=True, blank=True)

    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name=("Modified date"), auto_now=True)


class usersFeedback(models.Model):
    name = models.CharField(max_length=45, null=True, blank=True, verbose_name="Name",default="Anonymous")
    email = models.CharField(max_length=45, verbose_name="Email",null=True,blank=True)
    user_feedback = models.TextField(verbose_name="Feedback")

    creation_date = models.DateTimeField(verbose_name="Creation date", auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name="Modified date", auto_now=True)

    def __unicode__(self):
        name = " "
        try:
            name += self.name
        except Exception,e:
            pass
        try:
            name += self.email
        except Exception, e:
            pass
        return name


class requestNewPerson(models.Model):
    name = models.CharField(max_length=45, null=True, blank=True, verbose_name="Your Name", default="Anonymous")
    email = models.CharField(max_length=45, verbose_name="Email",blank=True,null=True)
    request_new = models.CharField(max_length=255,verbose_name="Request New Person Name")
    request_hints = models.TextField(verbose_name="Any Hints? ",blank=True,null=True)

    creation_date = models.DateTimeField(verbose_name="Creation date", auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name="Modified date", auto_now=True)

    def __unicode__(self):
        name = " "
        try:
            name += self.name
        except Exception,e:
            pass
        try:
            name += " // "+self.email
        except Exception, e:
            pass
	try:
	    name += " // "+self.request_new
	except Exception,e:
	    pass
        return name
