from django.conf.urls import patterns, include, url

urlpatterns = patterns('feedback.views',
                       url(r'^report/$', 'image_report', name='image_report'),
                       url(r'^report/thanks/$', 'thanks_report', name='thanks_report'),

                       url(r'user_feedback/$', 'user_feedback', name='user_feedback'),
                       url(r'request_new/$', 'request_new', name='request_new')
)
