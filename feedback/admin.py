from django.contrib import admin
from feedback.models import ImageReporting

from django import forms
from sorl.thumbnail import default
ADMIN_THUMBS_SIZE = '40x40'
ADMIN_THUMBS_SIZE_LARGE = '100x100'

class ImageReportingForm(forms.ModelForm):
	long_reason = forms.CharField(widget=forms.Textarea)	
	short_reason = forms.CharField(widget=forms.Textarea)

	class Meta:
		model = ImageReporting

class ImageReportingAdmin(admin.ModelAdmin):
	form = ImageReportingForm
	fields = ('image_thumb','image','name', 'email','long_reason','short_reason',)
	list_display = ['image_thumb', 'name' ]
	readonly_fields=('image_thumb',)
	def image_thumb(self,obj):
		if(obj.image.imagefile):
			try:
				thumb = default.backend.get_thumbnail(obj.image.imagefile.file, ADMIN_THUMBS_SIZE_LARGE)
				return u'<img width="%s" src="%s" />' % (thumb.width, thumb.url)
			except Exception, e:
				pass
		else:
			pass

	image_thumb.short_description = 'Image '
	image_thumb.allow_tags = True	

admin.site.register(ImageReporting, ImageReportingAdmin)


from feedback.models import requestNewPerson
from feedback.models import usersFeedback

admin.site.register(requestNewPerson)
admin.site.register(usersFeedback)