__author__ = 'w4rlock'

from django.forms import ModelForm
from captcha.fields import ReCaptchaField
from feedback.models import usersFeedback


class userFeedbackForm(ModelForm):
    captcha = ReCaptchaField()

    class Meta:
        model = usersFeedback
        exclude = ['creation_date', 'modified_date']