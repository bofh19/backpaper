from django.forms import ModelForm,IntegerField
from captcha.fields import ReCaptchaField
from feedback.models import ImageReporting


class ImageReportingForm(ModelForm):
	captcha = ReCaptchaField()
	image_id = IntegerField()
	class Meta:
		model = ImageReporting
		exclude=['creation_date','modified_date','image']
