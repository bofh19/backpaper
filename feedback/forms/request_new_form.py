__author__ = 'w4rlock'

from django.forms import ModelForm
from captcha.fields import ReCaptchaField
from feedback.models import requestNewPerson


class requestNewPersonForm(ModelForm):
    captcha = ReCaptchaField()

    class Meta:
        model = requestNewPerson
        exclude = ['creation_date', 'modified_date']