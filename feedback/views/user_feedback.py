__author__ = 'w4rlock'

from django.http import HttpResponseRedirect
from feedback.forms import userFeedbackForm
from django.shortcuts import render_to_response
from django.template import RequestContext

def user_feedback(request):
    if request.method == 'POST':
        form = userFeedbackForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            current_feedback = form.save(commit=False)
            try:
                current_feedback.save()
                return HttpResponseRedirect('/feedback/report/thanks/')
            except Exception, e:
                pass
    else:
        form = userFeedbackForm()

    return render_to_response(
        'feedback/templates/user_feedback.html',
        {
            'form': form
        },
        context_instance=RequestContext(request))
