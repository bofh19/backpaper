
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.forms.forms import NON_FIELD_ERRORS

from feedback.forms import ImageReportingForm

from images.models import name_images


def image_report(request):
    name_image = ''
    if request.method == 'POST':
        form = ImageReportingForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            # print data
        try:
            imageReport = form.save(commit=False)
            imageReport.image = name_images.objects.get(id=form.cleaned_data['image_id'])
            imageReport.save()
            return HttpResponseRedirect('/feedback/report/thanks/')
        except Exception, e:
            try:
                name_image = name_images.objects.get(id=request.POST['image_id'])
            except Exception, e:
                print e
                form._errors[NON_FIELD_ERRORS] = form.error_class(
                    ['No Proper Image Id Given ', 'Please Come to This Page The Right Way'])
    else:
        try:
            form = ImageReportingForm()
            form.fields["image_id"].initial = request.GET['img_id']
            name_image = name_images.objects.get(id=request.GET['img_id'])
        except Exception, e:
            # print e
            form = ImageReportingForm()
            form.full_clean()
            form._errors[NON_FIELD_ERRORS] = form.error_class(
                ['No Proper Image Id Given', 'Please Come to This Page The Right Way'])

    return render_to_response(
        'feedback/templates/image_report.html',
        {
            'name_image': name_image,
            'form': form
        },
        context_instance=RequestContext(request))


def thanks_report(request):
    return render_to_response(
        'feedback/templates/thanks_report.html',
        {
        },
        context_instance=RequestContext(request))