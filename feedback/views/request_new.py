__author__ = 'w4rlock'

from django.http import HttpResponseRedirect
from feedback.forms import requestNewPersonForm
from django.shortcuts import render_to_response
from django.template import RequestContext


def request_new(request):
    if request.method == 'POST':
        form = requestNewPersonForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            current_feedback = form.save(commit=False)
            try:
                current_feedback.save()
                return HttpResponseRedirect('/feedback/report/thanks/')
            except Exception, e:
                pass
    else:
        form = requestNewPersonForm()

    return render_to_response(
        'feedback/templates/request_new.html',
        {
            'form': form
        },
        context_instance=RequestContext(request))
